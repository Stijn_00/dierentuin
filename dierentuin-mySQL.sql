drop database dierentuin;
create database dierentuin;

use dierentuin;

CREATE TABLE  dier (
	id_dier_pk        	integer       primary key auto_increment,
	dier_name	varchar(30)	  not null
);

INSERT INTO  dier VALUES (1,'Indische olifant');
INSERT INTO  dier VALUES (2,'savanne olifant');
INSERT INTO  dier VALUES (3,'komodovaraan');
INSERT INTO  dier VALUES (4,'vogelspin');
INSERT INTO  dier VALUES (5,'stokstaartje');
INSERT INTO  dier VALUES (6,'ringstaartmaki');
INSERT INTO  dier VALUES (7,'doodshoofdaapje');
INSERT INTO  dier VALUES (8,'witkopmaki');
INSERT INTO  dier VALUES (9,'grijskopmaki');
INSERT INTO  dier VALUES (10,'reuzenpanda');
INSERT INTO  dier VALUES (11,'rode panda');
INSERT INTO  dier VALUES (12,'Amerikaanse zwarte beer');
INSERT INTO  dier VALUES (13,'pinguin');
INSERT INTO  dier VALUES (14,'slechtvalk');

CREATE TABLE natural_habitat (
   id_habitat_pk         integer     primary key auto_increment,
   habitat_name		varchar(30)	not null
);

INSERT INTO natural_habitat VALUES (1,'India');
INSERT INTO natural_habitat VALUES (2,'savanne');
INSERT INTO natural_habitat VALUES (3,'Indonesië');
INSERT INTO natural_habitat VALUES (4,'wereldwijd');
INSERT INTO natural_habitat VALUES (5,'Zuid-Afrika');
INSERT INTO natural_habitat VALUES (6,'Madagaskar');
INSERT INTO natural_habitat VALUES (7,'China');
INSERT INTO natural_habitat VALUES (8,'Hymalaya');
INSERT INTO natural_habitat VALUES (9,'Noord-Amerika');
INSERT INTO natural_habitat VALUES (10,'zuidelijk-halfrond');
INSERT INTO natural_habitat VALUES (11,'Latijns-Amerika');


CREATE TABLE familie (
   id_familie_pk         integer     primary key auto_increment,
   familie_name		varchar(30)	not null
);

INSERT INTO familie VALUES (1,'Elephantidae');
INSERT INTO familie VALUES (2,'Varanidae');
INSERT INTO familie VALUES (3,'Theraphosidae');
INSERT INTO familie VALUES (4,'Herpestidae');
INSERT INTO familie VALUES (5,'Lemuridae');
INSERT INTO familie VALUES (6,'Cebidae');
INSERT INTO familie VALUES (7,'Ursidae');
INSERT INTO familie VALUES (8,'Ailuridae');
INSERT INTO familie VALUES (9,'Spheniscidae');
INSERT INTO familie VALUES (10,'Falconidae');

CREATE TABLE zoo (
   id_zoo_pk         integer     primary key auto_increment,
   zoo_name		varchar(30)	not null
);

INSERT INTO zoo VALUES (1,'hier');
INSERT INTO zoo VALUES (2,'zoo van Berlijn');
INSERT INTO zoo VALUES (3,'pairi daiza');
INSERT INTO zoo VALUES (4,'Dublin zoo');
INSERT INTO zoo VALUES (5,'Antwerpse zoo');

CREATE TABLE woonplaats (
   id_woonplaats_pk     integer     primary key auto_increment,
   woonplaats_name		varchar(30)	not null,
   id_zoo		integer,
   foreign key (id_zoo) references zoo(id_zoo_pk)
);

INSERT INTO woonplaats VALUES (1,'savanne', 1);
INSERT INTO woonplaats VALUES (2,'apendorp', 1);
INSERT INTO woonplaats VALUES (3,'wilde westen', 1);
INSERT INTO woonplaats VALUES (4,'zee', 1);
INSERT INTO woonplaats VALUES (5,'pandaverblijf', 1);
INSERT INTO woonplaats VALUES (6,'reptielverblijf', 1);
INSERT INTO woonplaats VALUES (7,'vogelweide', 1);
INSERT INTO woonplaats VALUES (8,'verhuisd', 2);
INSERT INTO woonplaats VALUES (9,'verhuisd', 3);
INSERT INTO woonplaats VALUES (10,'verhuisd', 4);
INSERT INTO woonplaats VALUES (11,'verhuisd', 5);

CREATE TABLE gif (
   id_gif_pk         integer     primary key auto_increment,
   gif_werking		varchar(30)	not null
);

INSERT INTO gif VALUES (1,'niet giftig');
INSERT INTO gif VALUES (2,'pijnlijk');
INSERT INTO gif VALUES (3,'verlammend');
INSERT INTO gif VALUES (4,'dodelijk');

CREATE TABLE geslacht (
   id_geslacht_pk         integer     primary key auto_increment,
   geslacht		varchar(30)	not null
);

INSERT INTO geslacht VALUES (1,'mannelijk');
INSERT INTO geslacht VALUES (2,'vrouwelijk');
INSERT INTO geslacht VALUES (3,'onbepaald');


CREATE TABLE dierentuin (
   id_pk               integer       primary key auto_increment,
   id_dier             integer,
   geboorte_datum   datetime not null,
   sterf_datum      datetime null,
   id_familie       integer,
   id_woonplaats    integer,
   id_habitat       integer,
   id_gif           integer,
   dieren_naam      varchar(30) not null,
   id_geslacht      integer not null,

foreign key (id_dier) references  dier(id_dier_pk),
foreign key (id_familie) references familie(id_familie_pk),
foreign key (id_woonplaats) references woonplaats(id_woonplaats_pk),
foreign key (id_habitat) references natural_habitat(id_habitat_pk),
foreign key (id_gif) references gif(id_gif_pk),
foreign key (id_geslacht) references geslacht(id_geslacht_pk)
);

INSERT INTO dierentuin VALUES (1,1,'2018-04-25',null,1,1,1,1, "Kaymook", 2);
INSERT INTO dierentuin VALUES (2,1,'2017-12-25',null,1,1,1,1, "Mumba", 1);
INSERT INTO dierentuin VALUES (3,2,'2015-02-25',null,1,1,2,1, "Maxime", 1);
INSERT INTO dierentuin VALUES (4,2,'2017-04-14',null,1,1,2,1, "Lana", 2);
INSERT INTO dierentuin VALUES (5,3,'2017-02-25','9999-12-31',2,6,3,4, "Melodie", 2);
INSERT INTO dierentuin VALUES (6,3,'2015-04-14','2000-01-01',2,6,3,4, "Jonas", 1);
INSERT INTO dierentuin VALUES (7,4,'2009-06-21',null,3,6,4,4, "Spinna", 2);
INSERT INTO dierentuin VALUES (8,5,'2010-03-17',null,4,2,5,1, "Jonas", 1);
INSERT INTO dierentuin VALUES (9,5,'2010-05-25',null,4,2,5,1, "Frauke", 2);
INSERT INTO dierentuin VALUES (10,6,'2019-06-17',null,5,2,6,1, "Tino", 1);
INSERT INTO dierentuin VALUES (11,6,'2018-04-14',null,5,2,6,1, "Luna", 2);
INSERT INTO dierentuin VALUES (12,7,'2000-01-24',null,6,2,11,1, "Hugo", 1);
INSERT INTO dierentuin VALUES (13,7,'2005-09-17',null,6,2,11,1, "Kristin", 2);
INSERT INTO dierentuin VALUES (14,8,'2008-11-18',null,5,2,6,1, "James", 1);
INSERT INTO dierentuin VALUES (15,8,'2007-10-17',null,5,2,6,1, "Els", 2);
INSERT INTO dierentuin VALUES (16,9,'2009-05-05',null,5,2,6,1, "Jhon", 1);
INSERT INTO dierentuin VALUES (17,9,'2011-04-01',null,5,2,6,1, "Cloe", 2);
INSERT INTO dierentuin VALUES (18,10,'2010-09-17',null,7,5,7,1, "Marten", 1);
INSERT INTO dierentuin VALUES (19,10,'2012-07-05',null,7,5,7,1, "Hilde", 2);
INSERT INTO dierentuin VALUES (20,11,'2007-04-17',null,8,5,8,1, "Elvis", 1);
INSERT INTO dierentuin VALUES (21,11,'2005-03-06',null,8,5,8,1, "Ilse", 2);
INSERT INTO dierentuin VALUES (22,12,'2010-01-17',null,7,3,9,1, "Frans", 1);
INSERT INTO dierentuin VALUES (23,13,'2010-05-10',null,9,4,10,1, "Jos", 1);
INSERT INTO dierentuin VALUES (24,13,'2015-05-17',null,9,4,10,1, "Jose", 2);
INSERT INTO dierentuin VALUES (25,14,'2010-01-08',null,10,7,4,1, "Hedwig", 1);