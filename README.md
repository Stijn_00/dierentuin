# vrij project, dierentuin

## Dierentuin
Dit programma is gemaakt voor het makkelijk uit te lezen van data die in een vooropgegeven databank staan (games-mySQL.sql).

## Installatie

### Stap 1: installeer de databank
Dit programma maakt gebruik van een databank. Voor het geheel werkende te krijgen moet de databank eerst lokaal worden aangemaakt.
Dit doet men door naar het bestand diernetuin-mySQL.sql te zoeken en de inhoud ervan in de mySQL omgeving te plaatsen. Wanneer de databank nog niet is aangemaakt moet de bovenste lijn verwijderd worden (drop database).
Als vorige stappen zijn gebeurd laat men het commando runnen.

Vervolgens moet er nog een database gecreëerd worden namelijk een met de naam dierentuinTest.
Daarvoor moet men de stappen doen die vooraf vermeld zijn maar dan vervang je bij volgende commando's: drop database dierentuin; create database dierentuin; use dierentuin; dierentuin door dierentuinTest.

Als deze stappen zijn gedaan kan er voort gegaan worden naar de volgende stap, project openen met IDE.

### Stap 2: project openen met IDE
Als het programma geopend is met uw IDE dan kan het "pom" bestand openen, als u dat wilt (dit bestand staat rond README).
Daarna kunt u Maven (aan uw rechterkant) openklappen, daarna kunt u bij Lifecycle tegelijk clean en install selecteren, daarna op het groene driehoekje duwen.

##
Als stap een en twee gedaan zijn bent u klaar om dit programma te gebruiken.
Voor het programma te laten werken moet men wel zorgen dat de datank aanstaat gedurende de tijd dat de code is aan het runnen.

## Tests laten runnen
Voor de tests te laten runnen gaat men naar src, test, java.
Op de folder java klikt men (rechter muisklik) en selecteer men Run 'All Tests', dan worden alle tests getest.

Wanneer men de tests laat runnen gaat men een melding krijgen dat er een probleem is met test inserEenNieuweZoo(), dit kan verhelpen door een nieuwe zoo in te geven (hoofdmenu functie 8, niet gekende zoo), dan verschijnt dit probleem miet meer.

## Code laten runnen
Voor de code te laten runnen gaat men naar src, main, java, GamesApp.
In GamesApp vind je de mogelijkheid om het programma te laten starten (groen driehoekje).

Bij de optie "Wilt u teruggaan naaar het hoofdmenu?" moet men ja of nee zetten en 2 keer enteren voor acceptatie.


##
Veel plezier met de code

