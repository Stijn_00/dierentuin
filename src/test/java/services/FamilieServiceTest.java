package services;

import domain.Familie;
import exeptions.NoRecordFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.FamilieDAO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FamilieServiceTest {

    List<Familie> families = new ArrayList<>();
    Familie familie = new Familie();

    @InjectMocks
    FamilieService familieService;

    @Mock
    private FamilieDAO familieDAO;

    @Test
    void vindFamilieDoorId() throws NoRecordFoundException {
        when(familieDAO.vindAlleVerschillendeFamiliesDoorId(5)).thenReturn(familie);

        Familie result = familieService.vindFamilieDoorId(5);

        assertEquals(familie, result);
        verify(familieDAO, times(1)).vindAlleVerschillendeFamiliesDoorId(anyInt());
    }

    @Test
        //DoorId(2)).thenReturn(familie);
    void vindFamilieDoorIdEx() {
        assertThrows(NoRecordFoundException.class, () -> {
            when(familieDAO.vindAlleVerschillendeFamiliesDoorId(21231)).thenReturn(null);
            familieService.vindFamilieDoorId(21231);
        });
    }

    @Test
    void vindListfamilieDoorId() throws NoRecordFoundException {
        families.add(new Familie());
        when(familieDAO.vindAlleVerschillendeFamilies()).thenReturn(families);

        List<Familie> result = familieService.vindListfamilieDoorId();

        verify(familieDAO, times(1)).vindAlleVerschillendeFamilies();
        assertEquals(families, result);
    }

    @Test
        //Families()).thenReturn(families);
    void vindListfamilieDoorIdEx() {
        families.add(new Familie());
        assertThrows(NoRecordFoundException.class, () -> {
            when(familieDAO.vindAlleVerschillendeFamilies()).thenReturn(Collections.emptyList());
            familieService.vindListfamilieDoorId();
        });
    }
}