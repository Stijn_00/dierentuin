package services;

import domain.Zoo;
import exeptions.NoRecordFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.ZooDAO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ZooServiceTest {

    List<Zoo> zoos = new ArrayList<>();
    Zoo zoo = new Zoo();

    @InjectMocks
    ZooService zooService;

    @Mock
    private ZooDAO zooDAO;

    @Test
    void vindZooDoorId() throws NoRecordFoundException {
        when(zooDAO.vindAlleVerschillendeZooDoorId(1)).thenReturn(zoo);

        Zoo result = zooService.vindZooDoorId(1);

        assertEquals(zoo, result);
        verify(zooDAO, times(1)).vindAlleVerschillendeZooDoorId(anyInt());
    }

    @Test
        //DoorId(1)).thenReturn(zoo);
    void vindFamilieDoorIdEx() {
        assertThrows(NoRecordFoundException.class, () -> {
            when(zooDAO.vindAlleVerschillendeZooDoorId(25)).thenReturn(null);
            zooService.vindZooDoorId(25);
        });
    }

    @Test
    void vindListZooDoorId() throws NoRecordFoundException {
        zoos.add(new Zoo());
        when(zooDAO.vindAlleVerschillendeZoos()).thenReturn(zoos);

        List<Zoo> result = zooService.vindListZooDoorId();

        verify(zooDAO, times(1)).vindAlleVerschillendeZoos();
        assertEquals(zoos, result);
    }


    @Test
        //Zoos()).thenReturn(zoos);
    void vindListZooDoorIdEx() {
        zoos.add(new Zoo());
        assertThrows(NoRecordFoundException.class, () -> {
            when(zooDAO.vindAlleVerschillendeZoos()).thenReturn(Collections.emptyList());
            zooService.vindListZooDoorId();
        });
    }

    @Test
    void vindzooPk() throws NoRecordFoundException {
        when(zooDAO.vindzooPk("hier")).thenReturn(1);

        int result = zooService.vindzooPk("hier");

        assertEquals(1, result);
        verify(zooDAO, times(1)).vindzooPk(anyString());
    }

    @Test
        //Pk("hier")).thenReturn(1);
    void vindzooPkEx() {
        assertThrows(NoRecordFoundException.class, () -> {
            when(zooDAO.vindzooPk("Oostende")).thenReturn(0);
            zooService.vindzooPk("Oostende");
        });
    }
}