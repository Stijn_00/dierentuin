package services;

import domain.Gif;
import exeptions.NoRecordFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.GifDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GifServiceTest {

    Gif gif = new Gif();

    @InjectMocks
    GifService gifService;

    @Mock
    private GifDAO gifDAO;

    @Test
    void vindGifDoorId() throws NoRecordFoundException {
        when(gifDAO.vindAlleVerschillendeGifsDoorId(3)).thenReturn(gif);

        Gif result = gifService.vindGifDoorId(3);

        assertEquals(gif, result);
        verify(gifDAO, times(1)).vindAlleVerschillendeGifsDoorId(anyInt());
    }

    @Test
        //DoorId(5)).thenReturn(gif);
    void vindGifDoorIdEx() {
        assertThrows(NoRecordFoundException.class, () -> {
            when(gifDAO.vindAlleVerschillendeGifsDoorId(2134)).thenReturn(null);
            gifService.vindGifDoorId(2134);
        });
    }
}