package services;

import domain.Woonplaats;
import exeptions.NoRecordFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.WoonplaatsDAO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WoonplaatsServiceTest {

    List<Woonplaats> woonplaatsen = new ArrayList<>();
    Woonplaats woonplaats = new Woonplaats();

    @InjectMocks
    WoonplaatsService woonplaatsService;

    @Mock
    private WoonplaatsDAO woonplaatsDAO;

    @Test
    void vindWoonplaatsDoorId() throws NoRecordFoundException {
        when(woonplaatsDAO.vindAlleVerschillendeWoonplaatsDoorId(4)).thenReturn(woonplaats);

        Woonplaats result = woonplaatsService.vindWoonplaatsDoorId(4);

        assertEquals(woonplaats, result);
        verify(woonplaatsDAO, times(1)).vindAlleVerschillendeWoonplaatsDoorId(anyInt());
    }

    @Test
        //DoorId(2)).thenReturn(woonplaats);
    void vindWoonplaatsDoorIdEx() {
        assertThrows(NoRecordFoundException.class, () -> {
            when(woonplaatsDAO.vindAlleVerschillendeWoonplaatsDoorId(21231)).thenReturn(null);
            woonplaatsService.vindWoonplaatsDoorId(21231);
        });
    }

    @Test
    void vindListWoonplaatsDoorId() throws NoRecordFoundException {
        woonplaatsen.add(new Woonplaats());
        when(woonplaatsDAO.vindAlleVerschillendeWoonplaatsen()).thenReturn(woonplaatsen);

        List<Woonplaats> result = woonplaatsService.vindListWoonplaatsDoorId();

        verify(woonplaatsDAO, times(1)).vindAlleVerschillendeWoonplaatsen();
        assertEquals(woonplaatsen, result);
    }

    @Test
        //Woonplaatsen()).thenReturn(woonplaatsen);
    void vindListWoonplaatsDoorIdEx() {
        woonplaatsen.add(new Woonplaats());
        assertThrows(NoRecordFoundException.class, () -> {
            when(woonplaatsDAO.vindAlleVerschillendeWoonplaatsen()).thenReturn(Collections.emptyList());
            woonplaatsService.vindListWoonplaatsDoorId();
        });
    }

    @Test
    void vindWoonplaatsZooId() throws NoRecordFoundException {
        when(woonplaatsDAO.vindWoonplaatsZooId(4)).thenReturn(10);

        int result = woonplaatsService.vindWoonplaatsZooId(4);

        assertEquals(10, result);
        verify(woonplaatsDAO, times(1)).vindWoonplaatsZooId(anyInt());
    }

    @Test
        //Id(2)).thenReturn(8);
    void vindWoonplaatsZooIdEx() {
        assertThrows(NoRecordFoundException.class, () -> {
            when(woonplaatsDAO.vindWoonplaatsZooId(10)).thenReturn(0);
            woonplaatsService.vindWoonplaatsZooId(10);
        });
    }
}