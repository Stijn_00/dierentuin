package services;

import domain.Dierentuin;
import exeptions.NoRecordFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.DierentuinDAO;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DierentuinServiceTest {

    List<Dierentuin> dierentuins = new ArrayList<>();
    Dierentuin dierentuin = new Dierentuin();

    @InjectMocks
    DierentuinService dierentuinService;

    @Mock
    private DierentuinDAO dierentuinDAO;

    @Test
    void vindDierDoorId() throws NoRecordFoundException {
        when(dierentuinDAO.vindListAlleVerschillendeDierenNamen(anyInt())).thenReturn(dierentuin);

        Dierentuin result = dierentuinService.vindDierDoorId(5);

        assertEquals(dierentuin, result);
        verify(dierentuinDAO, times(1)).vindListAlleVerschillendeDierenNamen(anyInt());
    }

    @Test
    void vindDierDoorIdEx() {
        assertThrows(NoRecordFoundException.class, () -> {
            when(dierentuinDAO.vindListAlleVerschillendeDierenNamen(2135)).thenReturn(null);
            dierentuinService.vindDierDoorId(2135);
        });
    }

    @Test
    void vindListVerschillendeDierenVanEenDierensoort() throws NoRecordFoundException {
        dierentuins.add(new Dierentuin());
        when(dierentuinDAO.vindVerschillendeDierenVanEenDierensoort("olifant")).thenReturn(dierentuins);

        List<Dierentuin> result = dierentuinService.vindListVerschillendeDierenVanEenDierensoort("olifant");

        verify(dierentuinDAO, times(1)).vindVerschillendeDierenVanEenDierensoort(anyString());
        assertEquals(dierentuins, result);
    }

    @Test
        //thenReturn(Arrays.asList(dierentuin));
    void vindListVerschillendeDierenVanEenDierensoortEx() {
        assertThrows(NoRecordFoundException.class, () -> {
            when(dierentuinDAO.vindVerschillendeDierenVanEenDierensoort("jknvsj")).thenReturn(Collections.emptyList());
            dierentuinService.vindListVerschillendeDierenVanEenDierensoort("jknvsj");
        });
    }

    @Test
        //thenReturn(Arrays.asList(dierentuin));
    void vindListVerschillendeDierenVanEenDierensoortEx2() {
        assertThrows(NoRecordFoundException.class, () -> {
            when(dierentuinDAO.vindVerschillendeDierenVanEenDierensoort("")).thenReturn(Collections.emptyList());
            dierentuinService.vindListVerschillendeDierenVanEenDierensoort("");
        });
    }

    @Test
    void vindDiervanafXGeboortedatum() throws NoRecordFoundException {
        dierentuins.add(new Dierentuin());
        LocalDate date1 = LocalDate.of(2018, 10, 25);
        when(dierentuinDAO.vindDiervanafXGeboortedatum(Date.valueOf(date1))).thenReturn(dierentuins);

        List<Dierentuin> result = dierentuinService.vindDiervanafXGeboortedatum(Date.valueOf(date1));

        verify(dierentuinDAO, times(1)).vindDiervanafXGeboortedatum(Date.valueOf(date1));
        assertEquals(dierentuins, result);
    }

    @Test
        //latenf len reteurn Arrays.asList(dierentuin)
    void vindDiervanafXGeboortedatumEx() {
        LocalDate date1 = LocalDate.of(1853, 10, 25);
        assertThrows(NoRecordFoundException.class, () -> {
            when(dierentuinDAO.vindDiervanafXGeboortedatum(Date.valueOf(date1))).thenReturn(Collections.emptyList());
            dierentuinService.vindDiervanafXGeboortedatum(Date.valueOf(date1));
        });
    }

    @Test
    void vindDiervanafXGetorvenzijn() throws NoRecordFoundException {
        dierentuins.add(new Dierentuin());
        LocalDate date1 = LocalDate.of(2018, 10, 25);
        when(dierentuinDAO.vindDiervanafXGestorvenZijn(Date.valueOf(date1))).thenReturn(dierentuins);

        List<Dierentuin> result = dierentuinService.vindDiervanafXGetorvenzijn(Date.valueOf(date1));

        verify(dierentuinDAO, times(1)).vindDiervanafXGestorvenZijn(Date.valueOf(date1));
        assertEquals(dierentuins, result);
    }

    @Test
        //thenReturn(Arrays.asList(dierentuin));
    void vindDiervanafXGetorvenzijnEx() {
        LocalDate date1 = LocalDate.of(1000, 10, 25);
        assertThrows(NoRecordFoundException.class, () -> {
            when(dierentuinDAO.vindDiervanafXGestorvenZijn(Date.valueOf(date1))).thenReturn(Collections.emptyList());
            dierentuinService.vindDiervanafXGetorvenzijn(Date.valueOf(date1));
        });
    }
}