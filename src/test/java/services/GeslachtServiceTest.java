package services;

import domain.Geslacht;
import exeptions.NoRecordFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.GeslachtDAO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GeslachtServiceTest {

    List<Geslacht> geslachts = new ArrayList<>();
    Geslacht geslacht  = new Geslacht();

    @InjectMocks
    GeslachtService geslachtService;

    @Mock
    private GeslachtDAO geslachtDAO;

    @Test
    void vindGeslachtDoorId() throws NoRecordFoundException {
        when(geslachtDAO.vindAlleVerschillendeGeslachtsDoorId(5)).thenReturn(geslacht);

        Geslacht result = geslachtService.vindGeslachtDoorId(5);

        assertEquals(geslacht, result);
        verify(geslachtDAO, times(1)).vindAlleVerschillendeGeslachtsDoorId(anyInt());
    }

    @Test
        //GeslachtsDoorId(4)).thenReturn(geslacht);
    void vindGeslachtDoorIdEx() {
        assertThrows(NoRecordFoundException.class, () -> {
            when(geslachtDAO.vindAlleVerschillendeGeslachtsDoorId(4)).thenReturn(null);
            geslachtService.vindGeslachtDoorId(4);
        });
    }
}