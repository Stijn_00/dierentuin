package services;

import domain.Dier;
import exeptions.NoRecordFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.DierDAO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DierServiceTest {

    List<Dier> dieren = new ArrayList<>();
    Dier dier = new Dier();

    @InjectMocks
    DierService dierService;

    @Mock
    private DierDAO dierDAO;

    @Test
    void vindDierDoorId() throws NoRecordFoundException {
        when(dierDAO.vindAlleVerschillendeDierenDoorId(5)).thenReturn(dier);

        Dier result = dierService.vindDierDoorId(5);

        assertEquals(dier, result);
        verify(dierDAO, times(1)).vindAlleVerschillendeDierenDoorId(anyInt());
    }

    @Test
        //DoorId(2)).thenReturn(dier);
    void vindDierDoorIdEx() {
        assertThrows(NoRecordFoundException.class, () -> {
            when(dierDAO.vindAlleVerschillendeDierenDoorId(2123)).thenReturn(null);
            dierService.vindDierDoorId(2123);
        });
    }

    @Test
    void vindListDierDoorId() throws NoRecordFoundException {
        dieren.add(new Dier());
        when(dierDAO.vindAlleVerschillendeDieren()).thenReturn(dieren);

        List<Dier> result = dierService.vindListDierDoorId();

        verify(dierDAO, times(1)).vindAlleVerschillendeDieren();
        assertEquals(dieren, result);
    }

    @Test
        //Dieren()).thenReturn(dieren);
    void vindListDierDoorIdEx() {
        dieren.add(new Dier());
        assertThrows(NoRecordFoundException.class, () -> {
            when(dierDAO.vindAlleVerschillendeDieren()).thenReturn(Collections.emptyList());
            dierService.vindListDierDoorId();
        });
    }

    @Test
    void vindListVerschillendeDieren() throws NoRecordFoundException {
        dieren.add(new Dier());
        when(dierDAO.vindVerschillendeDieren("olifant")).thenReturn(dieren);

        List<Dier> result = dierService.vindListVerschillendeDieren("olifant");

        verify(dierDAO, times(1)).vindVerschillendeDieren(anyString());
        assertEquals(dieren, result);
    }

    @Test
        //Dieren("olifant")).thenReturn(dieren);
    void vindListVerschillendeDierenEx() {
        dieren.add(new Dier());
        assertThrows(NoRecordFoundException.class, () -> {
            when(dierDAO.vindVerschillendeDieren("njsvnjksdfvn")).thenReturn(Collections.emptyList());
            dierService.vindListVerschillendeDieren("njsvnjksdfvn");
        });
    }

    @Test
    void vindEenDierenDoorBevat() throws NoRecordFoundException {
        when(dierDAO.vindEenDierenDoorBevat("komodovaraan")).thenReturn(3);

        int result = dierService.vindEenDierenDoorBevat("komodovaraan");

        assertEquals(3, result);
        verify(dierDAO, times(1)).vindEenDierenDoorBevat(anyString());
    }

    @Test
        //Bevat("komodovaraan")).thenReturn(3);
    void vindEenDierenDoorBevatEx() {
        assertThrows(NoRecordFoundException.class, () -> {
            when(dierDAO.vindEenDierenDoorBevat("njvdj")).thenReturn(0);
            dierService.vindEenDierenDoorBevat("njvdj");
        });
    }

}