package services;

import domain.Natural_Habitat;
import exeptions.NoRecordFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.Natural_HabitatDAO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class Natural_HabitatServiceTest {

    List<Natural_Habitat> naturalHabitats = new ArrayList<>();
    Natural_Habitat naturalHabitat = new Natural_Habitat();

    @InjectMocks
    Natural_HabitatService naturalHabitatService;

    @Mock
    private Natural_HabitatDAO naturalHabitatDAO;

    @Test
    void vindHabitatDoorId() throws NoRecordFoundException {
        when(naturalHabitatDAO.vindAlleVerschillendeHabitatsDoorId(4)).thenReturn(naturalHabitat);

        Natural_Habitat result = naturalHabitatService.vindHabitatDoorId(4);

        assertEquals(naturalHabitat, result);
        verify(naturalHabitatDAO, times(1)).vindAlleVerschillendeHabitatsDoorId(anyInt());
    }

    @Test
        //DoorId(21231)).thenReturn(naturalHabitat);
    void vindHabitatDoorIdEx() {
        assertThrows(NoRecordFoundException.class, () -> {
            when(naturalHabitatDAO.vindAlleVerschillendeHabitatsDoorId(21231)).thenReturn(null);
            naturalHabitatService.vindHabitatDoorId(21231);
        });
    }

    @Test
    void vindListHabitatsDoorId() throws NoRecordFoundException {
        naturalHabitats.add(new Natural_Habitat());
        when(naturalHabitatDAO.vindAlleVerschillendeHabitats()).thenReturn(naturalHabitats);

        List<Natural_Habitat> result = naturalHabitatService.vindListHabitatsDoorId();

        verify(naturalHabitatDAO, times(1)).vindAlleVerschillendeHabitats();
        assertEquals(naturalHabitats, result);
    }

    @Test
        //Habitats()).thenReturn(naturalHabitats);
    void vindListHabitatsDoorIdEx() {
        naturalHabitats.add(new Natural_Habitat());
        assertThrows(NoRecordFoundException.class, () -> {
            when(naturalHabitatDAO.vindAlleVerschillendeHabitats()).thenReturn(Collections.emptyList());
            naturalHabitatService.vindListHabitatsDoorId();
        });
    }
}