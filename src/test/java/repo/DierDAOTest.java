package repo;

import domain.Dier;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import static database.MySqlConnection.*;

class DierDAOTest {

    private Connection con;
    private Dier dier = new Dier();
    private DierDAO dierDAO;

    @BeforeEach
    public void connectionToDB() {
        try {
            con = createConnectionTest(con);
            dierDAO = new DierDAO(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        dier.setId_dier_pk(5);
        dier.setDier_name("stokstaartje");
    }

    @AfterEach
    public  void closeDB(){
        closeConnectionsTest(con);
    }

    @Test
    void vindAlleVerschillendeDierenDoorId() {
        Dier result = dierDAO.vindAlleVerschillendeDierenDoorId(5);
        Assertions.assertEquals(dier.toString(), result.toString());
    }

    @Test
    void vindAlleVerschillendeDierenDoorIdNull() {
        Dier result = dierDAO.vindAlleVerschillendeDierenDoorId(515321561);
        Assertions.assertEquals(null, result);
    }

    @Test
    void vindAlleVerschillendeDieren() {
        List<Dier> result = dierDAO.vindAlleVerschillendeDieren();

        Assertions.assertFalse(result.isEmpty());
    }

    @Test
    void vindVerschillendeDieren() {
        List<Dier> result = dierDAO.vindVerschillendeDieren("olifant");

        Assertions.assertFalse(result.isEmpty());
    }

    @Test
    void vindVerschillendeDierenNull() {
        List<Dier> result = dierDAO.vindVerschillendeDieren("azerty");

        Assertions.assertEquals(Collections.EMPTY_LIST, result);
    }

    @Test
    void vindEenDierenDoorBevat() {
        int result = dierDAO.vindEenDierenDoorBevat("stokstaartje");
        Assertions.assertEquals(dier.getId_dier_pk(), result);
    }

    @Test
    void vindEenDierenDoorBevatNull() {
        int result = dierDAO.vindEenDierenDoorBevat("zzjncjk");
        Assertions.assertEquals(0, result);
    }
}