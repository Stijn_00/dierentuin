package repo;

import domain.Familie;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static database.MySqlConnection.closeConnectionsTest;
import static database.MySqlConnection.createConnectionTest;

class FamilieDAOTest {

    private Connection con;
    private Familie familie = new Familie();
    private FamilieDAO familieDAO;

    @BeforeEach
    public void connectionToDB() {
        try {
            con = createConnectionTest(con);
            familieDAO = new FamilieDAO(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        familie.setId_familie_pk(6);
        familie.setFamilie_name("Cebidae");
    }

    @AfterEach
    public  void closeDB(){
        closeConnectionsTest(con);
    }

    @Test
    void vindAlleVerschillendeFamiliesDoorId() {
        Familie result = familieDAO.vindAlleVerschillendeFamiliesDoorId(6);
        Assertions.assertEquals(familie.toString(), result.toString());
    }

    @Test
    void vindAlleVerschillendeFamiliesDoorIdNull() {
        Familie result = familieDAO.vindAlleVerschillendeFamiliesDoorId(615132);

        Assertions.assertEquals(null, result);
    }

    @Test
    void vindAlleVerschillendeFamilies() {
        List<Familie> result = familieDAO.vindAlleVerschillendeFamilies();

        Assertions.assertFalse(result.isEmpty());
    }
}