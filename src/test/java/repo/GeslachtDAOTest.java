package repo;

import domain.Geslacht;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static database.MySqlConnection.closeConnectionsTest;
import static database.MySqlConnection.createConnectionTest;
import static org.junit.jupiter.api.Assertions.*;

class GeslachtDAOTest {
    private Connection con;
    private Geslacht geslacht = new Geslacht();
    private GeslachtDAO geslachtDAO;

    @BeforeEach
    public void connectionToDB() {
        try {
            con = createConnectionTest(con);
            geslachtDAO = new GeslachtDAO(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        geslacht.setId_geslacht_pk(2);
        geslacht.setGeslacht_name("vrouwelijk");
    }

    @AfterEach
    public  void closeDB(){
        closeConnectionsTest(con);
    }

    @Test
    void vindAlleVerschillendeGeslachtsDoorId() {
        Geslacht result = geslachtDAO.vindAlleVerschillendeGeslachtsDoorId(2);
        Assertions.assertEquals(geslacht.toString(), result.toString());
    }

    @Test
    void vindAlleVerschillendeGeslachtsDoorIdNull() {
        Geslacht result = geslachtDAO.vindAlleVerschillendeGeslachtsDoorId(213102);
        Assertions.assertEquals(null, result);
    }

    @Test
    void vindAlleVerschillendeGeslachts() {
        List<Geslacht> result = geslachtDAO.vindAlleVerschillendeGeslachts();

        Assertions.assertFalse(result.isEmpty());
    }
}