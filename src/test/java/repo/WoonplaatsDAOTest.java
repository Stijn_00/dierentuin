package repo;

import domain.Dierentuin;
import domain.Woonplaats;
import domain.Zoo;
import exeptions.NoRecordFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import services.ZooService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static database.MySqlConnection.closeConnectionsTest;
import static database.MySqlConnection.createConnectionTest;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;

class WoonplaatsDAOTest {

    private Connection con;
    private WoonplaatsDAO woonplaatsDAO;
    private Woonplaats woonplaats = new Woonplaats();

    @BeforeEach
    public void connectionToDB() {
        try {
            con = createConnectionTest(con);
            woonplaatsDAO = new WoonplaatsDAO(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        woonplaats.setId_woonplaats_pk(5);
        woonplaats.setWoonplaats_name("pandaverblijf");

        ZooService zooService = new ZooService();
        Zoo zoo = null;
        try {
            zoo = zooService.vindZooDoorId(1);
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        woonplaats.setZoo(zoo);
    }

    private void assertData(Woonplaats expected, Woonplaats actual) {
        assertAll("assert data from borrower", () -> assertEquals(expected.getId_woonplaats_pk(), actual.getId_woonplaats_pk()),
                () -> assertEquals(expected.getWoonplaats_name(), actual.getWoonplaats_name()),
                () -> assertEquals(expected.getZoo().getZoo_name(), actual.getZoo().getZoo_name()),
                () -> assertEquals(expected.getZoo().getId_zoo_pk(), actual.getZoo().getId_zoo_pk())
        );
    }

    @AfterEach
    public void closeDB() {
        closeConnectionsTest(con);
    }

    @Test
    void vindAlleVerschillendeWoonplaatsDoorId() {
        Woonplaats result = woonplaatsDAO.vindAlleVerschillendeWoonplaatsDoorId(5);
        assertData(woonplaats, result);
    }

    @Test
    void vindAlleVerschillendeWoonplaatsDoorIdNull() {
        Woonplaats result = woonplaatsDAO.vindAlleVerschillendeWoonplaatsDoorId(1512315);
        Assertions.assertEquals(null, result);
    }

    @Test
    void vindAlleVerschillendeWoonplaatsen() {
        List<Woonplaats> result = woonplaatsDAO.vindAlleVerschillendeWoonplaatsen();

        Assertions.assertFalse(result.isEmpty());
    }

    @Test
    void vindWoonplaatsZooId() {
        int result = woonplaatsDAO.vindWoonplaatsZooId(4);
        Assertions.assertEquals(10, result);
    }

    @Test
    void inserEenNieuweZoo() {
        Woonplaats begin = woonplaatsDAO.vindAlleVerschillendeWoonplaatsDoorId(13);

        woonplaatsDAO.inserEenNieuweZoo(7);

        Woonplaats einde = woonplaatsDAO.vindAlleVerschillendeWoonplaatsDoorId(13);

        Assertions.assertNotEquals(begin, einde);
    }
}