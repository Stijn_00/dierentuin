package repo;

import domain.*;
import exeptions.NoRecordFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import services.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import static database.MySqlConnection.closeConnectionsTest;
import static database.MySqlConnection.createConnectionTest;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;

class DierentuinDAOTest {

    private Connection con;
    private DierentuinDAO dierentuinDAO;

    @BeforeEach
    public void connectionToDB() {
        try {
            con = createConnectionTest(con);
            dierentuinDAO = new DierentuinDAO(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public  void closeDB(){
        closeConnectionsTest(con);
    }

    private void assertData(Dierentuin expected, Dierentuin actual) {
        assertAll("assert data from borrower", () -> assertEquals(expected.getId_pk(), actual.getId_pk()),
                () -> assertEquals(expected.getDier().getId_dier_pk(), actual.getDier().getId_dier_pk()),
                () -> assertEquals(expected.getDier().getDier_name(), actual.getDier().getDier_name()),
                () -> assertEquals(expected.getGeboorte_datum(), actual.getGeboorte_datum()),
                () -> assertEquals(expected.getSterf_datum(), actual.getSterf_datum()),
                () -> assertEquals(expected.getWoonplaats().getWoonplaats_name(), actual.getWoonplaats().getWoonplaats_name()),
                () -> assertEquals(expected.getWoonplaats().getId_woonplaats_pk(), actual.getWoonplaats().getId_woonplaats_pk()),
                () -> assertEquals(expected.getWoonplaats().getZoo().getId_zoo_pk(), actual.getWoonplaats().getZoo().getId_zoo_pk()),
                () -> assertEquals(expected.getWoonplaats().getZoo().getZoo_name(), actual.getWoonplaats().getZoo().getZoo_name()),
                () -> assertEquals(expected.getFamilie().getFamilie_name(), actual.getFamilie().getFamilie_name()),
                () -> assertEquals(expected.getFamilie().getId_familie_pk(), actual.getFamilie().getId_familie_pk()),
                () -> assertEquals(expected.getGif().getGif_werking(), actual.getGif().getGif_werking()),
                () -> assertEquals(expected.getGif().getId_gif_pk(), actual.getGif().getId_gif_pk()),
                () -> assertEquals(expected.getNatural_habitat().getHabitat_name(), actual.getNatural_habitat().getHabitat_name()),
                () -> assertEquals(expected.getNatural_habitat().getId_habitat_pk(), actual.getNatural_habitat().getId_habitat_pk())
        );
    }

    @Test
    void vindListAlleVerschillendeDierenNamen() {
        Dierentuin dierentuin = new Dierentuin();
        LocalDate date1 = LocalDate.of(2017, 2, 25);
        LocalDate date2 = LocalDate.of(9999, 12, 31);

        dierentuin.setId_pk(5);
        dierentuin.setDieren_naam("Melodie");
        dierentuin.setGeboorte_datum(Date.valueOf(date1));
        dierentuin.setSterf_datum(Date.valueOf(date2));

        DierService dierService = new DierService();
        Dier dier = null;
        try {
            dier = dierService.vindDierDoorId(3);
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        dierentuin.setDier(dier);

        FamilieService familieService = new FamilieService();
        Familie fam = null;
        try {
            fam = familieService.vindFamilieDoorId(2);
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        dierentuin.setFamilie(fam);

        WoonplaatsService woonplaatsService = new WoonplaatsService();
        Woonplaats wp = null;
        try {
            wp = woonplaatsService.vindWoonplaatsDoorId(6);
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        dierentuin.setWoonplaats(wp);

        Natural_HabitatService naturalHabitatService = new Natural_HabitatService();
        Natural_Habitat nh = null;
        try {
            nh = naturalHabitatService.vindHabitatDoorId(3);
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        dierentuin.setNatural_habitat(nh);

        GeslachtService geslachtService = new GeslachtService();
        Geslacht gesl = null;
        try {
            gesl = geslachtService.vindGeslachtDoorId(2);
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        dierentuin.setGeslacht(gesl);

        GifService gifService = new GifService();
        Gif gif = null;
        try {
            gif = gifService.vindGifDoorId(4);
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        dierentuin.setGif(gif);

        DierentuinDAO dao = new DierentuinDAO(con);
        Dierentuin result = dao.vindListAlleVerschillendeDierenNamen(5);

        assertData(dierentuin, result);
    }

    @Test
    void vindVerschillendeDierenVanEenDierensoort() {
        List<Dierentuin> result = dierentuinDAO.vindVerschillendeDierenVanEenDierensoort("olifant");

        Assertions.assertFalse(result.isEmpty());
    }

    @Test
    void vindVerschillendeDierenVanEenDierensoortNull() {
        List<Dierentuin> result = dierentuinDAO.vindVerschillendeDierenVanEenDierensoort("nsdgnjkvng");

        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void vindDiervanafXGeboortedatum() {
        LocalDate date1 = LocalDate.of(2018, 10, 25);

        List<Dierentuin> result = dierentuinDAO.vindDiervanafXGeboortedatum(Date.valueOf(date1));

        Assertions.assertFalse(result.isEmpty());
    }

    @Test
    void vindDiervanafXGeboortedatumNull() {
        LocalDate date1 = LocalDate.of(1, 1, 01);

        List<Dierentuin> result = dierentuinDAO.vindDiervanafXGeboortedatum(Date.valueOf(date1));

        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void vindDiervanafXGestorvenZijn() {
        LocalDate date1 = LocalDate.of(2018, 10, 25);

        List<Dierentuin> result = dierentuinDAO.vindDiervanafXGestorvenZijn(Date.valueOf(date1));

        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void vindDiervanafXGestorvenZijnNull() {
        LocalDate date1 = LocalDate.of(2000, 1, 01);

        List<Dierentuin> result = dierentuinDAO.vindDiervanafXGestorvenZijn(Date.valueOf(date1));

        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void updateEenGeslachtVanEenDier() {
        Dierentuin begin = dierentuinDAO.vindListAlleVerschillendeDierenNamen(4);

        dierentuinDAO.updateEenGeslachtVanEenDier(3, 4);

        Dierentuin einde = dierentuinDAO.vindListAlleVerschillendeDierenNamen(4);

        Assertions.assertNotEquals(begin, einde);
    }

    @Test
    void updateEenWoonplaatsVanEenDier() {
        Dierentuin begin = dierentuinDAO.vindListAlleVerschillendeDierenNamen(6);

        dierentuinDAO.updateEenWoonplaatsVanEenDier(3, 6);

        Dierentuin einde = dierentuinDAO.vindListAlleVerschillendeDierenNamen(6);

        Assertions.assertNotEquals(begin, einde);
    }

    @Test
    void updateEenSterfdatumVanEenDier() {
        LocalDate date1 = LocalDate.of(2000, 1, 01);
        Dierentuin begin = dierentuinDAO.vindListAlleVerschillendeDierenNamen(8);

        dierentuinDAO.updateEenSterfdatumVanEenDier(8, Date.valueOf(date1));

        Dierentuin einde = dierentuinDAO.vindListAlleVerschillendeDierenNamen(8);

        Assertions.assertNotEquals(begin, einde);
    }

    @Test
    void insertEenGeboorte() {
        LocalDate date1 = LocalDate.of(2000, 1, 01);
        List<Dierentuin> begin = dierentuinDAO.vindDiervanafXGeboortedatum(Date.valueOf(date1));

        dierentuinDAO.insertEenGeboorte(8, Date.valueOf(date1), 4, 6, 9, 2, "jvn", 3);

        List<Dierentuin> einde = dierentuinDAO.vindDiervanafXGeboortedatum(Date.valueOf(date1));

        Assertions.assertNotEquals(begin, einde);
    }
}