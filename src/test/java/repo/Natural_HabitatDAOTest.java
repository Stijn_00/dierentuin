package repo;

import domain.Natural_Habitat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static database.MySqlConnection.closeConnectionsTest;
import static database.MySqlConnection.createConnectionTest;
import static org.junit.jupiter.api.Assertions.*;

class Natural_HabitatDAOTest {

    private Connection con;
    private Natural_Habitat natural_habitat = new Natural_Habitat();
    private Natural_HabitatDAO natural_habitatDAO;

    @BeforeEach
    public void connectionToDB() {
        try {
            con = createConnectionTest(con);
            natural_habitatDAO = new Natural_HabitatDAO(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        natural_habitat.setId_habitat_pk(2);
        natural_habitat.setHabitat_name("savanne");
    }

    @AfterEach
    public  void closeDB(){
        closeConnectionsTest(con);
    }

    @Test
    void vindAlleVerschillendeHabitatsDoorId() {
        Natural_Habitat result = natural_habitatDAO.vindAlleVerschillendeHabitatsDoorId(2);
        Assertions.assertEquals(natural_habitat.toString(), result.toString());
    }

    @Test
    void vindAlleVerschillendeHabitatsDoorIdNull() {
        Natural_Habitat result = natural_habitatDAO.vindAlleVerschillendeHabitatsDoorId(13135432);
        Assertions.assertEquals(null, result);
    }

    @Test
    void vindAlleVerschillendeHabitats() {
        List<Natural_Habitat> result = natural_habitatDAO.vindAlleVerschillendeHabitats();

        Assertions.assertFalse(result.isEmpty());
    }
}