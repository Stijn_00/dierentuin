package repo;

import domain.Zoo;
import exeptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import services.ZooService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static database.MySqlConnection.createConnectionTest;
import static org.junit.jupiter.api.Assertions.*;

class ZooDAOTest {

    private Connection con;
    private ZooDAO zooDAO;
    private Zoo zoo = new Zoo();

    @BeforeEach
    public void connectionToDB() {
        try {
            con = createConnectionTest(con);
            zooDAO = new ZooDAO(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        zoo.setId_zoo_pk(3);
        zoo.setZoo_name("pairi daiza");
    }

    @Test
    void vindAlleVerschillendeZooDoorId() {
        Zoo result = zooDAO.vindAlleVerschillendeZooDoorId(3);
        Assertions.assertEquals(zoo.toString(), result.toString());
    }

    @Test
    void vindAlleVerschillendeZooDoorIdNull() {
        Zoo result = zooDAO.vindAlleVerschillendeZooDoorId(3123121);
        Assertions.assertEquals(null, result);
    }

    @Test
    void vindAlleVerschillendeZoos() {
        List<Zoo> result = zooDAO.vindAlleVerschillendeZoos();

        Assertions.assertFalse(result.isEmpty());
    }

    @Test
    void inserEenNieuweZoo() {
        Zoo begin = zooDAO.vindAlleVerschillendeZooDoorId(6);

        zooDAO.inserEenNieuweZoo("Halle");

        Zoo einde = zooDAO.vindAlleVerschillendeZooDoorId(6);

        Assertions.assertNotEquals(begin, einde);
    }

    @Test
    void vindzooPk() {
        int result = zooDAO.vindzooPk("Halle");
        Assertions.assertEquals(6, result);
    }
}