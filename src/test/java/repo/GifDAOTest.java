package repo;

import domain.Gif;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static database.MySqlConnection.closeConnectionsTest;
import static database.MySqlConnection.createConnectionTest;
import static org.junit.jupiter.api.Assertions.*;

class GifDAOTest {

    private Connection con;
    private Gif gif = new Gif();
    private GifDAO gifDAO;

    @BeforeEach
    public void connectionToDB() {
        try {
            con = createConnectionTest(con);
            gifDAO = new GifDAO(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        gif.setId_gif_pk(3);
        gif.setGif_werking("verlammend");
    }

    @AfterEach
    public  void closeDB(){
        closeConnectionsTest(con);
    }

    @Test
    void vindAlleVerschillendeGifsDoorId() {
        Gif result = gifDAO.vindAlleVerschillendeGifsDoorId(3);
        Assertions.assertEquals(gif.toString(), result.toString());
    }

    @Test
    void vindAlleVerschillendeGifsDoorIdNull() {
        Gif result = gifDAO.vindAlleVerschillendeGifsDoorId(31151);
        Assertions.assertEquals(null, result);
    }

}