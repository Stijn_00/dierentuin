package domain;

/**
 * Dit is een klasse van de tabel woonplaats (in onze databank).
 */
public class Woonplaats {

    private int id_woonplaats_pk;
    private String woonplaats_name;
    private Zoo zoo;

    public Woonplaats() {

    }

    public int getId_woonplaats_pk() {
        return  this.id_woonplaats_pk;
    }


    public void setId_woonplaats_pk(int id_woonplaats_pk) {
        this.id_woonplaats_pk = id_woonplaats_pk;
    }

    public String getWoonplaats_name() {
        return woonplaats_name;
    }

    public void setWoonplaats_name(String woonplaats_name) {
        this.woonplaats_name = woonplaats_name;
    }

    public Zoo getZoo() {
        return zoo;
    }

    public void setZoo(Zoo zoo) {
        this.zoo = zoo;
    }

    @Override
    public String toString() {
        return "Woonplaats{" +
                "id_woonplaats_pk=" + id_woonplaats_pk +
                ", woonplaats_name='" + woonplaats_name + '\'' +
                ", zoo=" + zoo +
                '}';
    }
}
