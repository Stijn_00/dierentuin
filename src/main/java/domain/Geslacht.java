package domain;

/**
 * Dit is een klasse van de tabel geslacht (in onze databank).
 */
public class Geslacht {

    private int id_geslacht_pk;
    private String geslacht_name;

    public Geslacht(int i, String vrouwelijk) {
        this.id_geslacht_pk = id_geslacht_pk;
        this.geslacht_name = geslacht_name;
    }

    public Geslacht() {

    }

    public void setId_geslacht_pk(int id_geslacht_pk) {
        this.id_geslacht_pk = id_geslacht_pk;
    }

    public String getGeslacht_name() {
        return geslacht_name;
    }

    public void setGeslacht_name(String geslacht_name) {
        this.geslacht_name = geslacht_name;
    }

    @Override
    public String toString() {
        return "Geslacht{" +
                "id_geslacht_pk=" + id_geslacht_pk +
                ", geslacht_name='" + geslacht_name + '\'' +
                '}';
    }
}