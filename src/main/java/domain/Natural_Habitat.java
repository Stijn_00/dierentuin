package domain;

/**
 * Dit is een klasse van de tabel natural_habitat (in onze databank).
 */
public class Natural_Habitat {

    private int id_habitat_pk;
    private String habitat_name;

    public Natural_Habitat(int i, String indonesië) {
        this.id_habitat_pk = id_habitat_pk;
        this.habitat_name = habitat_name;
    }

    public Natural_Habitat() {

    }

    public int getId_habitat_pk() {
        return id_habitat_pk;
    }

    public void setId_habitat_pk(int id_habitat_pk) {
        this.id_habitat_pk = id_habitat_pk;
    }

    public String getHabitat_name() {
        return habitat_name;
    }

    public void setHabitat_name(String habitat_name) {
        this.habitat_name = habitat_name;
    }

    @Override
    public String toString() {
        return "Natural_habitat{" +
                "id_habitat_pk=" + id_habitat_pk +
                ", habitat_name='" + habitat_name + '\'' +
                '}';
    }
}