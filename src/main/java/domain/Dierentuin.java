package domain;

import java.util.Date;

/**
 * Dit is een klasse van de tabel dierentuin (in onze databank).
 */
public class Dierentuin {

    private int id_pk;
    private Dier dier;
    private Date geboorte_datum;
    private Date sterf_datum;
    private Familie familie;
    private Woonplaats woonplaats;
    private Natural_Habitat Natural_habitat;
    private Gif gif;
    private String dieren_naam;
    private Geslacht geslacht;

    public Dierentuin() {
        this.id_pk = id_pk;
        this.dier = dier;
        this.geboorte_datum = geboorte_datum;
        this.sterf_datum = sterf_datum;
        this.familie = familie;
        this.woonplaats = woonplaats;
        this.Natural_habitat = Natural_habitat;
        this.gif = gif;
        this.dieren_naam = dieren_naam;
        this.geslacht = geslacht;
    }

    public int getId_pk() {
        return id_pk;
    }

    public void setId_pk(int id_pk) {
        this.id_pk = id_pk;
    }

    public Dier getDier() {
        return dier;
    }

    public void setDier(Dier dier) {
        this.dier = dier;
    }

    public Date getGeboorte_datum() {
        return geboorte_datum;
    }

    public void setGeboorte_datum(Date geboorte_datum) {
        this.geboorte_datum = geboorte_datum;
    }

    public Date getSterf_datum() {
        return sterf_datum;
    }

    public void setSterf_datum(Date sterf_datum) {
        this.sterf_datum = sterf_datum;
    }

    public Familie getFamilie() {
        return familie;
    }

    public void setFamilie(Familie familie) {
        this.familie = familie;
    }

    public Woonplaats getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(Woonplaats woonplaats) {
        this.woonplaats = woonplaats;
    }

    public Natural_Habitat getNatural_habitat() {
        return Natural_habitat;
    }

    public void setNatural_habitat(Natural_Habitat natural_habitat) {
        Natural_habitat = natural_habitat;
    }

    public Gif getGif() {
        return gif;
    }

    public void setGif(Gif gif) {
        this.gif = gif;
    }

    public String getDieren_naam() {
        return this.dieren_naam;
    }

    public void setDieren_naam(String dieren_naam) {
        this.dieren_naam = dieren_naam;
    }

    public Geslacht getGeslacht() {
        return geslacht;
    }

    public void setGeslacht(Geslacht geslacht) {
        this.geslacht = geslacht;
    }

    @Override
    public String toString() {
        return "Dierentuin{" +
                "id_pk=" + id_pk +
                ", dier=" + dier +
                ", geboorte_datum=" + geboorte_datum +
                ", sterf_datum=" + sterf_datum +
                ", familie=" + familie +
                ", woonplaats=" + woonplaats +
                ", Natural_habitat=" + Natural_habitat +
                ", gif=" + gif +
                ", dieren_naam='" + dieren_naam + '\'' +
                ", geslacht=" + geslacht +
                '}';
    }

    public void getDieren_naam(String dieren_naam) {
        this.dieren_naam = dieren_naam;
    }

}
