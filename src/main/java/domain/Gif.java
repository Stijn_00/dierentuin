package domain;

/**
 * Dit is een klasse van de tabel gif (in onze databank).
 */
public class Gif {

    private int id_gif_pk;
    private String gif_werking;

    public Gif(int i, String dodelijk) {
        this.id_gif_pk = id_gif_pk;
        this.gif_werking = gif_werking;
    }

    public Gif() {

    }

    public int getId_gif_pk() {
        return id_gif_pk;
    }

    public void setId_gif_pk(int id_gif_pk) {
        this.id_gif_pk = id_gif_pk;
    }

    public String getGif_werking() {
        return gif_werking;
    }

    public void setGif_werking(String gif_werking) {
        this.gif_werking = gif_werking;
    }

    @Override
    public String toString() {
        return "Gif{" +
                "id_gif_pk=" + id_gif_pk +
                ", gif_werking='" + gif_werking + '\'' +
                '}';
    }
}