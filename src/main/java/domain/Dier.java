package domain;

/**
 * Dit is een klasse van de tabel dier (in onze databank).
 */
public class Dier {

    private int id_dier_pk;
    private String dier_name;

    public Dier() {
    }

    public int getId_dier_pk() {
        return id_dier_pk;
    }

    public void setId_dier_pk(int id_dier_pk) {
        this.id_dier_pk = id_dier_pk;
    }

    public String getDier_name() {
        return this.dier_name;
    }

    public void setDier_name(String dier_name) {
        this.dier_name = dier_name;
    }

    @Override
    public String toString() {
        return "Dier{" +
                "id_dier_pk=" + id_dier_pk +
                ", dier_name='" + dier_name + '\'' +
                '}';
    }
}