package domain;

/**
 * Dit is een klasse van de tabel zoo (in onze databank).
 */
public class Zoo {

    private int id_zoo_pk;
    private String zoo_name;

    public Zoo() {
    }

    public int getId_zoo_pk() {
        return id_zoo_pk;
    }

    public void setId_zoo_pk(int id_zoo_pk) {
        this.id_zoo_pk = id_zoo_pk;
    }

    public String getZoo_name() {
        return zoo_name;
    }

    public void setZoo_name(String zoo_name) {
        this.zoo_name = zoo_name;
    }

    @Override
    public String toString() {
        return "Zoo{" +
                "id_zoo_pk=" + id_zoo_pk +
                ", zoo_name='" + zoo_name + '\'' +
                '}';
    }
}