package domain;

/**
 * Dit is een klasse van de tabel familie (in onze databank).
 */
public class Familie {

    private int id_familie_pk;
    private String familie_name;

    public Familie() {
        this.id_familie_pk = id_familie_pk;
        this.familie_name = familie_name;
    }

    public int getId_familie_pk() {
        return id_familie_pk;
    }

    public void setId_familie_pk(int id_familie_pk) {
        this.id_familie_pk = id_familie_pk;
    }

    public String getFamilie_name() {
        return familie_name;
    }

    public void setFamilie_name(String familie_name) {
        this.familie_name = familie_name;
    }

    @Override
    public String toString() {
        return "Familie{" +
                "id_familie_pk=" + id_familie_pk +
                ", familie_name='" + familie_name + '\'' +
                '}';
    }
}