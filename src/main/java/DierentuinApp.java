import application.DierentuinApplication;

public class DierentuinApp {

    /**
     * Java documentatie van DierentuinApp, dit is de klass waar de mainfunctie zich situeerd van dit project.
     *
     * @param args Standaard constructor in de main.
     * @author Stijn Hesters
     * @version 1.0
     */
    public static void main(String[] args) {
        DierentuinApplication dierentuinApplication = new DierentuinApplication();

        dierentuinApplication.start();
    }
}

