package application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

import static java.lang.System.*;

/**
 * Dit is de klas van de scanners (waar de verschillende scanners staan).
 */
public class Scanners {

    private static Scanner scanner;

    /**
     * chooseString is een methode voor een input van een user te vragen, dit is een string dat gevraagd wordt.
     *
     * @return Dit geeft een string weer, van wat de user input.
     */
    static String chooseString() {
        if (scanner == null) {
            scanner = new Scanner(in).useDelimiter("\n");
        }
        try {
            String string = scanner.next();
            return string;
        } catch (InputMismatchException e) {
            err.println("Er ging iets mis");
            chooseString();
        }
        return null;
    }

    /**
     * chooseNumber is een methode voor een input van een user te vragen, dit is een int dat gevraagd wordt.
     *
     * @return Dit geeft een int weer, van wat de user input (nummers), anders krijgt hij een InputMismatchException.
     */
    static int chooseNumber() {
        if (scanner == null) {
            scanner = new Scanner(in);
        }
        try {
            int number = scanner.nextInt();
            return number;
        } catch (InputMismatchException e) {
            err.println("Er ging iets mis, geef een geldig nummer in");
            chooseNumber();
        }
        return 0;
    }

    /**
     * chooseDateGeboorte is een methode voor een input van een user te vragen, dit is een string dat gevraagd wordt, daarna wordt het geparst naar een SimpledDateFormat.
     *
     * @return Dit geeft een SimpleDateFormat weer, van wat de user input (string van een datum), anders krijgt hij een InputMismatchException.
     */
    public static Date chooseDateGeboorte() {
        if (scanner == null) {
            scanner = new Scanner(in);
        }
        try {
            out.println("Geef een datum in van het formaat yyyy/MM/dd");
            String datum = scanner.next();
            return new SimpleDateFormat("yyyy/MM/dd").parse(datum);
        } catch (InputMismatchException | ParseException e) {
            err.println("Er ging iets mis, geef een geldige datum in");
            chooseDateGeboorte();
        }
        return null;
    }
}
