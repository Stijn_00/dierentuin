package application;

import domain.*;
import exeptions.NoRecordFoundException;
import services.*;

import java.util.*;

import static java.lang.System.*;

/**
 * Dit is de controller van dierentuinproject
 */

public class DierentuinApplication {

    private Map<Integer, String> menuMap = new HashMap<>();
    private DierentuinService dierentuinService = new DierentuinService();
    private DierService dierService = new DierService();
    private FamilieService familieService = new FamilieService();
    private GeslachtService geslachtService = new GeslachtService();
    private GifService gifService = new GifService();
    private Natural_HabitatService natural_HabitatService = new Natural_HabitatService();
    private WoonplaatsService woonplaatsService = new WoonplaatsService();
    private ZooService zooService = new ZooService();
    private Scanner scanner;
    private int geslachtNummer;


    /**
     * CreateMenu is voor het maken van lijst van elke mogelijke optie.
     */
    private void createMenu() {
        menuMap.put(0, "Sluit de applicatie.");
        menuMap.put(1, "Toon de verschillende diersoorten in het park.");
        menuMap.put(2, "Toon de verschillende diersoorten in het park, waar de gebruiker op kan selecteren.");
        menuMap.put(3, "Toon de verschillende familiesoorten die in het park aanwezig zijn.");
        menuMap.put(4, "Toon de verschillende natuurlijke habitats vanwaar de dieren afkomstig zijn.");
        menuMap.put(5, "Toon de verschillende leef plaatsen van een dier in de zoo.");
        menuMap.put(6, "Toon een lijst van dieren die voor ... geboren zijn.");
        menuMap.put(7, "Toon een lijst van dieren die voor ... gestorven zijn.");
        menuMap.put(8, "Een dier laten verhuizen naar een andere zoo.");
        menuMap.put(9, "De sterfdatum invoegen van een dier.");
        menuMap.put(10, "Een geboorte van een dier ingeven.");
        menuMap.put(11, "Een geslacht van een dier aanpassen.");
    }

    /**
     * Start is voor het samenstellen van de verschillende componenten voor de start.
     */
    public void start() {
        createMenu();
        out.println("---------------------Hallo bezoeker---------------------");
        out.println("Welkom in ons park");
        out.println("---------------------Hoofdmenu--------------------------");
        showMenu();
    }

    /**
     * showMenu is voor het uitprinten van alle menus.
     */
    private void showMenu() {
        for (Map.Entry<Integer, String> m : menuMap.entrySet()) {
            out.println(m.getKey() + " " + m.getValue());
        }
        out.println("--------------Dat waren de verschillende zaken die u kan kiezen");
        chooseOption();
    }

    /**
     * chooseOption is de optie die instaat voor het nummer de persoon kan kiezen en er naartoe gaat.
     */
    private void chooseOption() {
        if (scanner == null) {
            scanner = new Scanner(in);
        }
        try {
            out.println("Kies uw nummer: ");
            int i = scanner.nextInt();
            scanner.nextLine();
            executeOption(i);
        } catch (NoRecordFoundException e) {
            err.println("Dit is geen nummer, door u ingegeven");
            scanner.nextLine();
            chooseOption();
        }
    }

    /**
     * excexuteOption is een functie die kan switchen tussen de verschillende opties.
     *
     * @param option Deze input wordt door de user in een andere scanner mogelijk gemaakt, als een van de mogelijkheden matcht voert hij die case uit.
     * @throws NoRecordFoundException Als er geen case gaat uitgevoerd worden gaat hij een exceptie trowen naar de chooseOption en komt dit terecht in te catch.
     */
    protected void executeOption(int option) throws NoRecordFoundException {
        switch (option) {
            case 0:
                sluitDeApplicatie();
                break;
            case 1:
                toonDeVerschillendeDiersoortenInHetPark();
                break;
            case 2:
                toonDeVerschillendeDiersoortenInHetParkUserKeus();
                break;
            case 3:
                toonDeVerschillendeFamiliesoortenInHetPark();
                break;
            case 4:
                toonDeVerschillendeNatuurlijkeHabitatsVanwaarDeDierenAfkomstigZijn();
                break;
            case 5:
                toonDeVerschillendeLeefplaatsenVanZenDierInDeZoo();
                break;
            case 6:
                toonEenLijstVanDierenDieVanafXGeborenZijn();
                break;
            case 7:
                toonEenLijstVanDierenDieVanafXGestorvenZijn();
                break;
            case 8:
                eenDierLatenVerhuizenNaarEenAndereZoo();
                break;
            case 9:
                deSterfdatumInvoegenVanEenDier();
                break;
            case 10:
                eenGeboorteVanEenDierIngeven();
                break;
            case 11:
                eenGeslachtVanEenDierAanpassen();
                break;
        }
        scanner.close();
    }

    /**
     * eenGeslachtVanEenDierAanpassen is een methode die het mogelijk maakt voor de user om een geslacht aan te passen van een dier.
     */
    private void eenGeslachtVanEenDierAanpassen() {
        out.println("U koos voor de methode: een geslacht van een dier aanpassen");
        out.println("De verschillende diersoorten die we in ons park hebben:");
        verschillendeDiersoorten();
        out.println("Tot welk van bovenstaande soorten behoort het dier waarvan u het geslacht wilt aanpassen?");
        String bevatSoort = Scanners.chooseString().toLowerCase();

        out.println("De verschillende namen van dieren die we in ons park hebben van de vooropgegeven soort:");
        verschillendeDiernamenVanSoort(bevatSoort);
        out.println("Welke id heeft het dier waarvan u het geslacht wilt aanpassen?");
        int id = Scanners.chooseNumber();

        out.println("Naar welk geslacht wilt u het geslacht aanpassen? (M/V/O)");
        stringGeslachtOmzettenInNummer();

        dierentuinService.updateEenGeslachtVanEenDier(geslachtNummer, id);
        out.println("Kijkt u maar gebruiker het geslacht is met succes geupdate.");
        verschillendeDiernamenVanSoort(bevatSoort);

        endQuestion();

    }

    /**
     * stringGeslachtOmzettenInNummer is een methode die een geslacht omzet naar het nummer die overeenkomt in de database met dat geslacht.
     */
    private void stringGeslachtOmzettenInNummer() {
        String bevatGeslacht = Scanners.chooseString().toLowerCase();

        if (bevatGeslacht.contains("m")) {
            geslachtNummer = 1;
        } else if (bevatGeslacht.contains("v")) {
            geslachtNummer = 2;
        } else if (bevatGeslacht.contains("o")) {
            geslachtNummer = 3;
        } else {
            out.println("U heeft geen geldig antwoord gegeven op vorige vraag, probeer het opnieuw");
            out.println("Geef een geldig M/V/O in");
            stringGeslachtOmzettenInNummer();
        }
    }

    /**
     * lijstVanAlleUitwisselingsdierentuinen is een methode die een lijst uitprint van alle gekende dierentuinen.
     */
    private void lijstVanAlleUitwisselingsdierentuinen() {
        try {
            List<Zoo> zoos = zooService.vindListZooDoorId();
            out.println("id nummer van de zoo \t\t Lijst van reeds gekende uitwisselingsdierentuinen");
            for (Zoo zoo : zoos) {
                out.println("(" + zoo.getId_zoo_pk() + " \t" + zoo.getZoo_name() + ")");
            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * verschillendeDiernamenVanSoort is een methode die een lijst van gegevens uitprint, dit van een bepaalde dierensoort.
     *
     * @param bevatSoort, dit is een input van de gebruiker, daarop wordt er gefilterd in wat de gebruiker te zien krijgt hier.
     */
    private void verschillendeDiernamenVanSoort(String bevatSoort) {
        try {
            List<Dierentuin> dierentuins = dierentuinService.vindListVerschillendeDierenVanEenDierensoort(bevatSoort);
            out.println("id dier \t diersoort \t geboortedatum \t verblijf \t naam van het dier \t geslacht");
            for (Dierentuin dierentuin : dierentuins) {
                out.println("(" + dierentuin.getId_pk() + "\t" + dierentuin.getDier().getDier_name() + " \t" + dierentuin.getGeboorte_datum() + " \t" + dierentuin.getWoonplaats().getWoonplaats_name() + "\t" + dierentuin.getDieren_naam() + "\t" + dierentuin.getGeslacht().getGeslacht_name() + ")");
            }
        } catch (NoRecordFoundException e) {
            e.printUserFriendlyMessage();
        }
    }

    /**
     * eenGeboorteVanEenDierIngeven is een metode die het mogelijk maakt voor de gebruiker een nieuw dier in te geven (van een diersoort die al in de dierentuin is).
     */
    private void eenGeboorteVanEenDierIngeven() {
        out.println("U koos voor de methode: een geboorte van een dier ingeven");

        out.println("De verschillende diersoorten die we in ons park hebben:");
        verschillendeDiersoorten();
        out.println("Tot welk van bovenstaande soorten behoort het dier dat is geboren?");
        String bevatSoort = Scanners.chooseString();
        int idDier = 0;
        try {
            idDier = dierService.vindEenDierenDoorBevat(bevatSoort);
        } catch (NoRecordFoundException e) {
            e.printUserFriendlyMessage();
        }
        int idFamilie = 0;
        int idWoonplaats = 0;
        int idHabitat = 0;
        int idGif = 0;

        if (idDier == 1) {
            idFamilie = 1;
            idWoonplaats = 1;
            idHabitat = 1;
            idGif = 1;
        } else if (idDier == 2) {
            idFamilie = 1;
            idWoonplaats = 1;
            idHabitat = 2;
            idGif = 1;
        } else if (idDier == 3) {
            idFamilie = 2;
            idWoonplaats = 6;
            idHabitat = 3;
            idGif = 4;
        } else if (idDier == 4) {
            idFamilie = 3;
            idWoonplaats = 6;
            idHabitat = 4;
            idGif = 4;
        } else if (idDier == 5) {
            idFamilie = 4;
            idWoonplaats = 2;
            idHabitat = 5;
            idGif = 1;
        } else if (idDier == 6) {
            idFamilie = 5;
            idWoonplaats = 2;
            idHabitat = 6;
            idGif = 1;
        } else if (idDier == 7) {
            idFamilie = 6;
            idWoonplaats = 2;
            idHabitat = 11;
            idGif = 1;
        } else if (idDier == 8) {
            idFamilie = 5;
            idWoonplaats = 2;
            idHabitat = 6;
            idGif = 1;
        } else if (idDier == 9) {
            idFamilie = 5;
            idWoonplaats = 2;
            idHabitat = 6;
            idGif = 1;
        } else if (idDier == 10) {
            idFamilie = 7;
            idWoonplaats = 5;
            idHabitat = 7;
            idGif = 1;
        } else if (idDier == 11) {
            idFamilie = 8;
            idWoonplaats = 5;
            idHabitat = 8;
            idGif = 1;
        } else if (idDier == 12) {
            idFamilie = 7;
            idWoonplaats = 3;
            idHabitat = 9;
            idGif = 1;
        } else if (idDier == 13) {
            idFamilie = 9;
            idWoonplaats = 4;
            idHabitat = 10;
            idGif = 1;
        } else if (idDier == 14) {
            idFamilie = 10;
            idWoonplaats = 3;
            idHabitat = 4;
            idGif = 1;
        } else {
            err.println("U heeft geen geldige soort ingegeven");
            out.println("U zal teruggaan naar hoofdmenu");
            start();
        }

        out.println("Wat is de geboortedatum van het nieuw geboren dier?");
        Date geboorteDatum = Scanners.chooseDateGeboorte();

        out.println("Wat is de naam van het nieuwe dier?");
        String dieren_naam = Scanners.chooseString();

        out.println("Welk geslacht heeft het nieuwe dier? (M/V/O)");
        stringGeslachtOmzettenInNummer();

        dierentuinService.insertEenGeboorte(idDier, geboorteDatum, idFamilie, idWoonplaats, idHabitat, idGif, dieren_naam, geslachtNummer);
        out.println("Kijk beste gebruiker het door u ingevoegen record staat nu in onze databank.");

        try {
            List<Dierentuin> dierentuins = dierentuinService.vindDiervanafXGeboortedatum(geboorteDatum);
            out.println("diersoort \t geboortedatum \t naam van het dier ");
            for (Dierentuin dierentuin : dierentuins) {
                out.println("(" + dierentuin.getDier().getDier_name() + " \t" + dierentuin.getGeboorte_datum() + "\t" + dierentuin.getDieren_naam() + ")");
            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }

        endQuestion();

    }

    /**
     * deSterfdatumInvoegenVanEenDier is een methode om een dier zijn sterfdatum in te geven (default staat de datum op null).
     */
    private void deSterfdatumInvoegenVanEenDier() {
        out.println("U koos voor de methode: de sterfdatum van een dier ingeven");

        out.println("De verschillende diersoorten die we in ons park hebben:");
        verschillendeDiersoorten();
        out.println("Tot welk van bovenstaande soorten behoort het dier dat is overleden?");
        String bevatSoort = Scanners.chooseString().toLowerCase();

        out.println("De verschillende namen van dieren die we in ons park hebben van de vooropgegeven soort:");
        verschillendeDiernamenVanSoort(bevatSoort);
        out.println("Welk id heeft het dier die is gestorven?");
        int id_pk = Scanners.chooseNumber();

        out.println("Geef de datum in wanneer een dier is overleden");
        Date datumSterfte = Scanners.chooseDateGeboorte();

        dierentuinService.updateEenSterfdatumVanEenDier(id_pk, datumSterfte);

        try {
            out.println(dierentuinService.vindDierDoorId(id_pk).getSterf_datum());
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        endQuestion();

    }

    /**
     * eenDierLatenVerhuizenNaarEenAndereZoo is een methode om de woonplaats van een dier naar verhuisd te zetten, in bijzaak daarvan wordt ook de id aangepast zodat men weet naar welke zo het dier is verhuisd.
     */
    private void eenDierLatenVerhuizenNaarEenAndereZoo() {
        out.println("U koos voor de methode: een dier laten verhuizen naar een andere zoo");
        out.println("De verschillende diersoorten die we in ons park hebben:");
        verschillendeDiersoorten();
        out.println("Tot welk van bovenstaande soorten behoort het dier dat gaat verhuizen?");
        String bevatSoort = Scanners.chooseString().toLowerCase();

        out.println("De verschillende namen van dieren die we in ons park hebben van de vooropgegeven soort:");
        verschillendeDiernamenVanSoort(bevatSoort);
        out.println("Welk id heeft het dier die gaat verhuizen?");
        int id_pk = Scanners.chooseNumber();

        out.println("Staat de dierentuin in de lijst van reeds gekende dierentuinen (J/N)?");
        lijstVanAlleUitwisselingsdierentuinen();
        jNZoo(id_pk);


    }

    /**
     * jNZoo is een methode die speciaal geschreven voor de methode eenDierLatenVerhuizenNaarEenAndereZoo en zo verder gaat naar verschillende methodes naargelang het antwoord op de vraag.
     *
     * @param id_pk, dit is een input van de user, voor te weten welk record straks gaat moeten aangepast worden.
     */
    private void jNZoo(int id_pk) {
        String jN = Scanners.chooseString().toLowerCase();
        if (jN.matches("ja") || jN.matches("j")) {
            zooBestaatAl(id_pk);
        } else if (jN.matches("n") || jN.matches("ne") || jN.matches("nee")) {
            aanmaakNieuweZoo(id_pk);
        } else {
            err.println("U heeft geen geldige input gegeven.");
            err.println("U zal teruggaan waar u kunt kiezen voor ja of nee");
            jNZoo(id_pk);
        }
    }

    /**
     * aanmaakNieuweZoo is een methode die een nieuwe zoo gaat aanmaken met het oog op het verhuizen van een dier.
     *
     * @param id_pk, dit is een input van de user, voor te weten welk record straks gaat moeten aangepast worden.
     */
    private void aanmaakNieuweZoo(int id_pk) {

        out.println("Wat is de naam van de nieuwe dierentuin waarnaar de verhuis gaat?");
        String nieuweZoo = Scanners.chooseString().toLowerCase();
        zooService.inserEenNieuweZoo(nieuweZoo);

        int zoo_woonplaats_id = 0;
        try {
            zoo_woonplaats_id = zooService.vindzooPk(nieuweZoo);
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }

        woonplaatsService.insertEenNieuweZoo(zoo_woonplaats_id);
        lijstVanAlleUitwisselingsdierentuinen();
        zooBestaatAl(id_pk);
    }

    /**
     * zooBestaatAl is een methode die vraagt naar de primary key van de tabel zoo, zo kan het record geupdate worden naar verhuisd in de diererntuintabel.
     *
     * @param id_pk, dit is een input van de user, voor te weten welk record straks gaat moeten aangepast worden.
     */
    private void zooBestaatAl(int id_pk) {
        out.println("Geef het id nummer in dat overeenkomt met het record van de zoo");
        int idZoo = Scanners.chooseNumber();

        int woonplaats_id = 0;
        try {
            woonplaats_id = woonplaatsService.vindWoonplaatsZooId(idZoo);
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        dierentuinService.updateEenWoonplaatsVanEenDier(woonplaats_id, id_pk);
        try {
            out.println(dierentuinService.vindDierDoorId(id_pk));
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        endQuestion();
    }

    /**
     * toonEenLijstVanDierenDieVanafXGestorvenZijn is een methode die een lijst van records weergeeft van dieren die voor een bepaalde datum gestorven zijn.
     */
    private void toonEenLijstVanDierenDieVanafXGestorvenZijn() {
        out.println("U koos voor de methode: toon een lijst van dieren die voor ... gestorven zijn");
        out.println("Geef de datum in van geboortes die voor ... gestorven zijn:");
        Date datumSterfte = Scanners.chooseDateGeboorte();

        try {
            List<Dierentuin> dierentuins = dierentuinService.vindDiervanafXGetorvenzijn(datumSterfte);
            out.println("diersoort \t geboortedatum \t sterfdatum \t naam van het dier ");
            for (Dierentuin dierentuin : dierentuins) {
                out.println("(" + dierentuin.getDier().getDier_name() + " \t" + dierentuin.getGeboorte_datum() + " \t" + dierentuin.getSterf_datum() + "\t" + dierentuin.getDieren_naam() + ")");
            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
            err.println("Er zijn vanaf de datum dat u hebt opgegeven geen sterftes geweest");
        }
        endQuestion();
    }

    /**
     * toonEenLijstVanDierenDieVanafXGeborenZijn is een methode die een lijst van records weergeeft van dieren die voor een bepaalde datum geboren zijn.
     */
    private void toonEenLijstVanDierenDieVanafXGeborenZijn() {
        out.println("U koos voor de methode: toon een lijst van dieren die voor ... geboren zijn");
        out.println("Geef de datum in van geboortes die voor ... geboren zijn:");
        Date datumGeboorte = Scanners.chooseDateGeboorte();

        try {
            List<Dierentuin> dierentuins = dierentuinService.vindDiervanafXGeboortedatum(datumGeboorte);
            out.println("diersoort \t geboortedatum \t naam van het dier ");
            for (Dierentuin dierentuin : dierentuins) {
                out.println("(" + dierentuin.getDier().getDier_name() + " \t" + dierentuin.getGeboorte_datum() + "\t" + dierentuin.getDieren_naam() + ")");
            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
            err.println("Er zijn vanaf de datum dat u hebt opgegeven geen geboortes geweest");
        }
        endQuestion();
    }

    /**
     * toonDeVerschillendeLeefplaatsenVanZenDierInDeZoo is een methode die uitprint welke delen we allemaal in het park hebben waar de dieren kunnen leven (dierenverblijven).
     */
    private void toonDeVerschillendeLeefplaatsenVanZenDierInDeZoo() {
        out.println("U koos voor de methode: toon de verschillende leef plaatsen van een dier in de zoo");
        try {
            out.println("leef plaatsen");
            List<Woonplaats> woonplaatsen = woonplaatsService.vindListWoonplaatsDoorId();
            for (Woonplaats woonplaats : woonplaatsen) {
                if (woonplaats.getWoonplaats_name().toLowerCase().matches("verhuisd")) {
                } else {
                    out.println(woonplaats.getWoonplaats_name());
                }
            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        endQuestion();
    }

    /**
     * toonDeVerschillendeNatuurlijkeHabitatsVanwaarDeDierenAfkomstigZijn is een methode die uitprint vanwaar de verschillende dieren van nature voorkomen.
     */
    private void toonDeVerschillendeNatuurlijkeHabitatsVanwaarDeDierenAfkomstigZijn() {
        out.println("U koos voor de methode: toon de verschillende natuurlijke habitats vanwaar de dieren afkomstig zijn");
        try {
            out.println("Natuurlijke habitat");
            List<Natural_Habitat> nhs = natural_HabitatService.vindListHabitatsDoorId();
            for (Natural_Habitat natural_habitat : nhs) {
                out.println(natural_habitat.getHabitat_name());
            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        endQuestion();
    }

    /**
     * toonDeVerschillendeFamiliesoortenInHetPark is een methode die de verschillende familienamen uitprint van alle verschillende dieren die in het park aanwezig zijn.
     */
    private void toonDeVerschillendeFamiliesoortenInHetPark() {
        out.println("U koos voor de methode: toon de verschillende familiesoorten die in het park aanwezig zijn");
        try {
            out.println("Familie naam");
            List<Familie> families = familieService.vindListfamilieDoorId();
            for (Familie familie : families) {
                out.println(familie.getFamilie_name());
            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        endQuestion();
    }

    /**
     * toonDeVerschillendeDiersoortenInHetParkUserKeus is een methode die een diersoort zoekt met een bepaalde specefiek deel in, dit specifiek deel is gekozen door de gebruiker.
     */
    private void toonDeVerschillendeDiersoortenInHetParkUserKeus() {
        List<Dier> dieren = Collections.emptyList();
        out.println("U koos voor de methode: toon de verschillende diersoorten in het park, waar de gebruiker op kan selecteren");
        out.println("Geef het deel van de naam in dat de diersoort moet bevatten?");
        String bevat = Scanners.chooseString().toLowerCase();
        try {
            out.println("Diersoort");
            dieren = dierService.vindListVerschillendeDieren(bevat);
            for (Dier dier : dieren) {
                out.println(dier.getDier_name());
            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        endQuestion();
    }

    /**
     * verschillendeDiersoorten is een methode die een lijst uitprint van alle verschillende diersoorten.
     *
     * @return Geeft een lijst van dieren (alles wat in de tabel diersoort staat) weer (alleen de diernamen).
     */
    private List<Dier> verschillendeDiersoorten() {
        out.println("Diersoort");
        try {
            List<Dier> dieren = dierService.vindListDierDoorId();
            for (Dier dier : dieren) {
                out.println(dier.getDier_name());
            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    /**
     * toonDeVerschillendeDiersoortenInHetPark is een methode die de verschillende diersoorten uitprint die in het park aanwezig zijn.
     */
    private void toonDeVerschillendeDiersoortenInHetPark() {
        out.println("U koos voor de methode: toon de verschillende diersoorten in het park");
        verschillendeDiersoorten();
        endQuestion();
    }

    /**
     * Deze methode print uit dat de applicatie is gesloten.
     */
    private void sluitDeApplicatie() {
        out.println("De applicatie is gesloten");
    }

    /**
     * endQuestion is een methode die vraagt dat u terug wilt gaan naar .
     */

    private void endQuestion() {

        out.println("Wilt u teruggaan naaar het hoofdmenu?");
        String repait = Scanners.chooseString().toLowerCase();

        if (repait.matches("yes") || repait.matches("ye") || repait.matches("y") || repait.matches("ja") || repait.matches("j")) {
            scanner.nextLine();
            start();
        } else if (repait.matches("no") || repait.matches("n") || repait.matches("nee") || repait.matches("nee")) {
            sluitDeApplicatie();
        } else {
            err.println("U heeft geen geldige input gegeven.");
            err.println("U zal teruggaan naar het hoofdmenu voor te kiezen");
            scanner.nextLine();
            start();
        }
    }
}
