package repo;


import domain.Dier;
import query_utils.QuerryDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static database.MySqlConnection.closeConnections;
import static database.MySqlConnection.createConnection;

/**
 * @author Stijn Hesters
 * Dit is de Database Access Object van de tabel dier in de MySQL Workbench.
 */

public class DierDAO {

    private Connection con;
    private ResultSet set;
    private PreparedStatement statement;

    public DierDAO() {
    }

    protected DierDAO(Connection con) {
        this.con = con;
    }

    /**
     * aggregateDier is een nieuwe methode waar al de namen van de (kolommen, in de tabel) dier in staan.
     *
     * @return Dit is voor alles te kunnen weergeven later dat in de tabel dier staat.
     */
    private Dier aggregateDier() {
        Dier dier = new Dier();
        try {
            dier.setId_dier_pk(set.getInt("id_dier_pk"));
            dier.setDier_name(set.getString("dier_name"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dier;
    }

    /**
     * vindAlleVerschillendeDierenDoorId is een methode dat een dierensoorten teruggeeft dat in het park leven / geleefd hebben, deze functie is voor aggregateDierentuin.
     *
     * @param dierenIdentificatie, deze parameter wordt ingevuld door aggregateDierentuin.
     * @return Geeft een record weer, namelijk het dierensoorten met het opgegeven identificatienummer.
     */
    public Dier vindAlleVerschillendeDierenDoorId(int dierenIdentificatie) {
        try {
            con = createConnection(con);
            statement = con.prepareStatement("Select * from dierentuin.dier WHERE id_dier_pk = ?");
            statement.setInt(1, dierenIdentificatie);
            statement.execute();
            set = statement.getResultSet();
            if (set.next()) {
                return this.aggregateDier();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return null;
    }

    /**
     * vindAlleVerschillendeDieren is een methode dat alle verschillende dierensoorten teruggeeft die in het park leven / geleefd hebben.
     *
     * @return Een lijst van al de verschillende dierensoorten die in het park leven / geleefd hebben.
     */

    public List<Dier> vindAlleVerschillendeDieren() {
        List<Dier> dieren = new ArrayList<>();
        try {
            con = createConnection(con);
            statement = con.prepareStatement(QuerryDAO.selecteerAll("dier"));
            statement.execute();
            set = statement.getResultSet();
            while (set.next()) {
                Dier dier;
                dier = this.aggregateDier();
                dieren.add(dier);
            }
            return dieren;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return Collections.emptyList();
    }

    /**
     * vindVerschillendeDieren is een methode om te kijken of een bepaald dierensoorten in het park is of niet.
     *
     * @param bevat, dit is een parameter die door de user is ingegeven, daarop wordt gezocht dat dit dierensoort er is of niet.
     * @return Een lijst van de dierensoorten die bevat in hun naam hebben.
     */
    public List<Dier> vindVerschillendeDieren(String bevat) {
        List<Dier> dieren = new ArrayList<>();
        try {
            con = createConnection(con);
            statement = con.prepareStatement("Select * from dierentuin.dier WHERE dier_name Like ?");
            statement.setString(1, "%" + bevat + "%");
            statement.execute();
            set = statement.getResultSet();
            while (set.next()) {
                Dier dier;
                dier = this.aggregateDier();
                dieren.add(dier);
            }
            return dieren;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return Collections.emptyList();
    }

    /**
     * vindEenDierenDoorBevat is een methode die Id_dier_pk weergeeft van een specifieke userinput.
     *
     * @param bevatSoort, dit is de userinput die bepaald welke Id_dier_pk er gaat worden weergegeven.
     * @return Een nummer, meer bepaald de primary key van deze tabel.
     */
    public int vindEenDierenDoorBevat(String bevatSoort) {
        try {
            con = createConnection(con);
            statement = con.prepareStatement("Select * from dierentuin.dier WHERE dier_name = ?");
            statement.setString(1, bevatSoort);
            statement.execute();
            set = statement.getResultSet();
            while (set.next()) {
                return this.aggregateDier().getId_dier_pk();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return 0;
    }
}
