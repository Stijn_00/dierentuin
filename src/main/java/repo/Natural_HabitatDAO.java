package repo;


import domain.Natural_Habitat;
import query_utils.QuerryDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static database.MySqlConnection.closeConnections;
import static database.MySqlConnection.createConnection;

/**
 * @author Stijn Hesters
 * Dit is de Database Access Object van de tabel natural_habitat in de MySQL Workbench.
 */

public class Natural_HabitatDAO {

    private Connection con;
    private ResultSet set;
    private PreparedStatement statement;

    public Natural_HabitatDAO() {
    }

    protected Natural_HabitatDAO(Connection con) {
        this.con = con;
    }

    /**
     * aggregateHabitat is een nieuwe methode waar al de namen van de (kolommen, in de tabel) natural_Habitat in staan.
     *
     * @return Dit is voor alles te kunnen weergeven later dat in de tabel natural_Habitat staat.
     */
    private Natural_Habitat aggregateHabitat() {
        Natural_Habitat habitat = new Natural_Habitat(3, "Indonesië");
        try {
            habitat.setId_habitat_pk(set.getInt("id_habitat_pk"));
            habitat.setHabitat_name(set.getString("habitat_name"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return habitat;
    }

    /**
     * vindAlleVerschillendeHabitatsDoorId is een methode dat een naam van een natuurlijke habitat teruggeeft dat van een dier is dat in het park leeft, deze functie is voor aggregateDierentuin.
     *
     * @param habitatIdentificatie, deze parameter wordt ingevuld door aggregateDierentuin.
     * @return Geeft een record weer, een naam van een natuurlijke habitat teruggeeft dat van een dier is dat in het park leeft met het opgegeven identificatienummer.
     */
    public Natural_Habitat vindAlleVerschillendeHabitatsDoorId(int habitatIdentificatie) {
        try {
            con = createConnection(con);
            statement = con.prepareStatement("Select * from dierentuin.natural_habitat WHERE id_habitat_pk = ?");
            statement.setInt(1, habitatIdentificatie);
            statement.execute();
            set = statement.getResultSet();
            if (set.next()) {
                return this.aggregateHabitat();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return null;
    }

    /**
     * vindAlleVerschillendeHabitats is een methode dat al de namen van de verschillende habitats teruggeeft dat mogelijk is van dieren in het park.
     *
     * @return Een lijst van al de namen van de verschillende natuurlijke habitat teruggeeft dat mogelijk is van dieren in het park.
     */

    public List<Natural_Habitat> vindAlleVerschillendeHabitats() {
        List<Natural_Habitat> habitats = new ArrayList<>();
        try {
            con = createConnection(con);
            statement = con.prepareStatement(QuerryDAO.selecteerAll("natural_habitat"));
            statement.execute();
            set = statement.getResultSet();
            while (set.next()) {
                Natural_Habitat habitat;
                habitat = this.aggregateHabitat();
                habitats.add(habitat);
            }
            return habitats;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return Collections.emptyList();
    }
}


