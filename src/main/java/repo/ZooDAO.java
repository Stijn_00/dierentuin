package repo;


import domain.Zoo;
import query_utils.QuerryDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static database.MySqlConnection.closeConnections;
import static database.MySqlConnection.createConnection;

/**
 * @author Stijn Hesters
 * Dit is de Database Access Object van de tabel zoo in de MySQL Workbench.
 */

public class ZooDAO {

    private Connection con;
    private ResultSet set;
    private PreparedStatement statement;

    public ZooDAO() {
    }

    protected ZooDAO(Connection con) {
        this.con = con;
    }

    /**
     * aggregateZoo is een nieuwe methode waar al de namen van de (kolommen, in de tabel) woonplaats in staan.
     *
     * @return Dit is voor alles te kunnen weergeven later dat in de tabel woonplaats staat.
     */
    private Zoo aggregateZoo() {
        Zoo zoo = new Zoo();
        try {
            zoo.setId_zoo_pk(set.getInt("id_zoo_pk"));
            zoo.setZoo_name(set.getString("zoo_name"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return zoo;
    }

    /**
     * vindAlleVerschillendeZooDoorId is een methode dat een zoo teruggeeft waarnaar een dier vertrekt, deze functie is voor aggregateWoonplaats.
     *
     * @param zooIdentificatie, deze parameter wordt ingevuld door aggregateZoo.
     * @return Geeft een record weer, namelijk de zoo met het opgegeven identificatienummer.
     */
    public Zoo vindAlleVerschillendeZooDoorId(int zooIdentificatie) {
        try {
            con = createConnection(con);
            statement = con.prepareStatement(QuerryDAO.selecteerAll("zoo") + " WHERE id_zoo_pk = ?");
            statement.setInt(1, zooIdentificatie);
            statement.execute();
            set = statement.getResultSet();
            if (set.next()) {
                return this.aggregateZoo();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return null;
    }

    /**
     * vindAlleVerschillendeZoos is een methode dat alle verschillende zoos teruggeeft waarnaar er dieren zijn weggegaan.
     *
     * @return Een lijst van al de verschillende zoos waarnaar er dieren zijn verhuisd.
     */

    public List<Zoo> vindAlleVerschillendeZoos() {
        List<Zoo> zoos = new ArrayList<>();
        try {
            con = createConnection(con);
            statement = con.prepareStatement(QuerryDAO.selecteerAll("zoo"));
            statement.execute();
            set = statement.getResultSet();
            while (set.next()) {
                Zoo zoo;
                zoo = this.aggregateZoo();
                zoos.add(zoo);
            }
            return zoos;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return Collections.emptyList();
    }

    /**
     * inserEenNieuweZoo is een methode die het voor de user mogelijk maakt om een nieuwe zoo in te geven.
     *
     * @param nieuweZoo, dit is de userinput namelijk de naam van een nieuwe zoo.
     */
    public void inserEenNieuweZoo(String nieuweZoo) {
        try {
            con = createConnection(con);
            statement = con.prepareStatement("INSERT INTO dierentuin.zoo  (zoo_name) VALUES ( ?)");
            statement.setString(1, nieuweZoo);
            statement.executeUpdate();
            System.out.println("U heeft het record met succes ingevoegd");
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * vindzooPk is een methode om van een bepaalde zoo de primary key te bepalen.
     *
     * @param nieuweZoo, dit is input van de user, namelijk daarop gaat men kijken naar de primary key van dat record.
     * @return een primary key van een door de user bepaalde record.
     */
    public int vindzooPk(String nieuweZoo) {
        try {
            con = createConnection(con);
            statement = con.prepareStatement("Select * from dierentuin.zoo WHERE zoo_name = ?");
            statement.setString(1, nieuweZoo);
            statement.execute();
            set = statement.getResultSet();
            if (set.next()) {
                return this.aggregateZoo().getId_zoo_pk();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return 0;
    }
}


