package repo;


import domain.Geslacht;
import query_utils.QuerryDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static database.MySqlConnection.closeConnections;
import static database.MySqlConnection.createConnection;

/**
 * @author Stijn Hesters
 * Dit is de Database Access Object van de tabel geslacht in de MySQL Workbench.
 */

public class GeslachtDAO {

    private Connection con;
    private ResultSet set;
    private PreparedStatement statement;

    public GeslachtDAO() {
    }

    protected GeslachtDAO(Connection con) {
        this.con = con;
    }

    /**
     * aggregateFamilie is een nieuwe methode waar al de namen van de (kolommen, in de tabel) familie in staan.
     *
     * @return Dit is voor alles te kunnen weergeven later dat in de tabel familie staat.
     */
    private Geslacht aggregateGeslacht() {
        Geslacht geslacht = new Geslacht(2, "vrouwelijk");
        try {
            geslacht.setId_geslacht_pk(set.getInt("id_geslacht_pk"));
            geslacht.setGeslacht_name(set.getString("geslacht"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return geslacht;
    }

    /**
     * vindAlleVerschillendeGeslachtsDoorId is een methode dat een geslacht teruggeeft dat in het park zijn, deze functie is voor aggregateDierentuin.
     *
     * @param geslachtIdentificatie, deze parameter wordt ingevuld door aggregateDierentuin.
     * @return Geeft een record weer, namelijk het geslacht met het opgegeven identificatienummer.
     */


    public Geslacht vindAlleVerschillendeGeslachtsDoorId(int geslachtIdentificatie) {
        try {
            con = createConnection(con);
            statement = con.prepareStatement("Select * from dierentuin.geslacht WHERE id_geslacht_pk = ?");
            statement.setInt(1, geslachtIdentificatie);
            statement.execute();
            set = statement.getResultSet();
            if (set.next()) {
                return this.aggregateGeslacht();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return null;
    }

    /**
     * vindAlleVerschillendeGeslachts is een methode dat alle verschillende geslachten teruggeeft die in het park mogelijk zijn.
     *
     * @return Een lijst van al de verschillende geslachten die mogelijk zijn in het park.
     */

    public List<Geslacht> vindAlleVerschillendeGeslachts() {
        List<Geslacht> geslachts = new ArrayList<>();
        try {
            con = createConnection(con);
            statement = con.prepareStatement(QuerryDAO.selecteerAll("geslacht"));
            statement.execute();
            set = statement.getResultSet();
            while (set.next()) {
                Geslacht geslacht;
                geslacht = this.aggregateGeslacht();
                geslachts.add(geslacht);
            }
            return geslachts;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return Collections.emptyList();
    }

}

