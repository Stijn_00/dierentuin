package repo;


import domain.Familie;
import query_utils.QuerryDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static database.MySqlConnection.closeConnections;
import static database.MySqlConnection.createConnection;

/**
 * @author Stijn Hesters
 * Dit is de Database Access Object van de tabel familie in de MySQL Workbench.
 */

public class FamilieDAO {

    private Connection con;
    private ResultSet set;
    private PreparedStatement statement;

    public FamilieDAO() {
    }

    protected FamilieDAO(Connection con) {
        this.con = con;
    }

    /**
     * aggregateFamilie is een nieuwe methode waar al de namen van de (kolommen, in de tabel) familie in staan.
     *
     * @return Dit is voor alles te kunnen weergeven later dat in de tabel familie staat.
     */
    private Familie aggregateFamilie() {
        Familie familie = new Familie();
        try {
            familie.setId_familie_pk(set.getInt("id_familie_pk"));
            familie.setFamilie_name(set.getString("familie_name"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return familie;
    }

    /**
     * vindAlleVerschillendeFamiliesDoorId is een methode dat een familie teruggeeft dat in het park zijn, deze functie is voor aggregateDierentuin.
     *
     * @param familieIdentificatie, deze parameter wordt ingevuld door aggregateDierentuin.
     * @return Geeft een record weer, namelijk het familie met het opgegeven identificatienummer.
     */
    public Familie vindAlleVerschillendeFamiliesDoorId(int familieIdentificatie) {
        try {
            con = createConnection(con);
            statement = con.prepareStatement("Select * from dierentuin.familie WHERE id_familie_pk = ?");
            statement.setInt(1, familieIdentificatie);
            statement.execute();
            set = statement.getResultSet();
            if (set.next()) {
                return this.aggregateFamilie();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return null;
    }

    /**
     * vindAlleVerschillendeFamilies is een methode dat alle verschillende families teruggeeft die in het park zijn.
     *
     * @return Een lijst van al de verschillende families die in het park zijn.
     */

    public List<Familie> vindAlleVerschillendeFamilies() {
        List<Familie> families = new ArrayList<>();
        try {
            con = createConnection(con);
            statement = con.prepareStatement(QuerryDAO.selecteerAll("familie"));
            statement.execute();
            set = statement.getResultSet();
            while (set.next()) {
                Familie familie;
                familie = this.aggregateFamilie();
                families.add(familie);
            }
            return families;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return Collections.emptyList();
    }
}

