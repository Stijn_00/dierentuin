package repo;


import domain.Gif;
import query_utils.QuerryDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static database.MySqlConnection.closeConnections;
import static database.MySqlConnection.createConnection;

/**
 * @author Stijn Hesters
 * Dit is de Database Access Object van de tabel gif in de MySQL Workbench.
 */

public class GifDAO {

    private Connection con;
    private ResultSet set;
    private PreparedStatement statement;

    public GifDAO() {
    }

    protected GifDAO(Connection con) {
        this.con = con;
    }

    /**
     * aggregateFamilie is een nieuwe methode waar al de namen van de (kolommen, in de tabel) familie in staan.
     *
     * @return Dit is voor alles te kunnen weergeven later dat in de tabel familie staat.
     */
    private Gif aggregateGif() {
        Gif gif = new Gif(4, "dodelijk");
        try {
            gif.setId_gif_pk(set.getInt("id_gif_pk"));
            gif.setGif_werking(set.getString("gif_werking"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return gif;
    }

    /**
     * vindAlleVerschillendeGifsDoorId is een methode dat teruggeeft dat is van een dier in het park giftig is of niet, deze functie is voor aggregateDierentuin.
     *
     * @param gifIdentificatie, deze parameter wordt ingevuld door aggregateDierentuin.
     * @return Geeft een record weer, namelijk dat het dier giftig is of niet en zijn gifIdentificatie.
     */

    public Gif vindAlleVerschillendeGifsDoorId(int gifIdentificatie) {
        try {
            con = createConnection(con);
            statement = con.prepareStatement("Select * from dierentuin.gif WHERE id_gif_pk = ?");
            statement.setInt(1, gifIdentificatie);
            statement.execute();
            set = statement.getResultSet();
            if (set.next()) {
                return this.aggregateGif();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return null;
    }

}


