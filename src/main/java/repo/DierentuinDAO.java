package repo;

import domain.*;
import exeptions.NoRecordFoundException;
import query_utils.QuerryDAO;
import services.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static database.MySqlConnection.closeConnections;
import static database.MySqlConnection.createConnection;

/**
 * @author Stijn Hesters
 * Dit is de Database Access Object van de tabel dierentuin in de MySQL Workbench.
 */

public class DierentuinDAO {

    private Connection con;
    private ResultSet set;
    private PreparedStatement statement;

    public DierentuinDAO() {
    }

    protected DierentuinDAO(Connection con) {
        this.con = con;
    }

    /**
     * insertQuerryForSaveUpdatesToZero is een functie die nodig is voor een querry te kunnen uitvoeren waarbij er een update gebeurt van een verwijzing naar een primary key.
     *
     * @throws SQLException Dit is voor als er een probleem is met de databank, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    protected void insertQuerryForSaveUpdatesToZero() throws SQLException {
        con = createConnection(con);
        statement = con.prepareStatement("SET SQL_SAFE_UPDATES=0;");
        statement.execute();
        set = statement.getResultSet();
    }

    /**
     * insertQuerryForSaveUpdatesToOne is een functie af te sluiten die nodig was voor een querry te kunnen uitvoeren waarbij er een update gebeurt van een verwijzing naar een primary key.
     *
     * @throws SQLException Dit is voor als er een probleem is met de databank, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    protected void insertQuerryForSaveUpdatesToOne() throws SQLException {
        con = createConnection(con);
        statement = con.prepareStatement("SET SQL_SAFE_UPDATES=1;");
        statement.execute();
        set = statement.getResultSet();
    }

    /**
     * aggregateDierentuin is een nieuwe methode waar al de namen van de (kolommen, in de tabel) dierentuin in staan.
     *
     * @return Dit is voor alles te kunnen weergeven later dat in de tabel dierentuin staat.
     */
    private Dierentuin aggregateDierentuin() {
        Dierentuin dierentuin = new Dierentuin();
        try {
            dierentuin.setId_pk(set.getInt("id_pk"));
            dierentuin.setGeboorte_datum(set.getDate("geboorte_datum"));
            dierentuin.setSterf_datum(set.getDate("sterf_datum"));
            dierentuin.getDieren_naam(set.getString("dieren_naam"));

            if (set.getInt("id_dier") > 0) {
                DierService dierService = new DierService();
                Dier dier = dierService.vindDierDoorId(set.getInt("id_dier"));
                dierentuin.setDier(dier);
            }

            if (set.getInt("id_familie") > 0) {
                FamilieService familieService = new FamilieService();
                Familie fam = familieService.vindFamilieDoorId(set.getInt("id_familie"));
                dierentuin.setFamilie(fam);
            }

            if (set.getInt("id_woonplaats") > 0) {
                WoonplaatsService woonplaatsService = new WoonplaatsService();
                Woonplaats wonp = woonplaatsService.vindWoonplaatsDoorId(set.getInt("id_woonplaats"));
                dierentuin.setWoonplaats(wonp);
            }

            if (set.getInt("id_habitat") > 0) {
                Natural_HabitatService habitatService = new Natural_HabitatService();
                Natural_Habitat habtat = habitatService.vindHabitatDoorId(set.getInt("id_habitat"));
                dierentuin.setNatural_habitat(habtat);
            }

            if (set.getInt("id_gif") > 0) {
                GifService gifService = new GifService();
                Gif gif = gifService.vindGifDoorId(set.getInt("id_gif"));
                dierentuin.setGif(gif);
            }

            if (set.getInt("id_geslacht") > 0) {
                GeslachtService geslachtService = new GeslachtService();
                Geslacht gesl = geslachtService.vindGeslachtDoorId(set.getInt("id_geslacht"));
                dierentuin.setGeslacht(gesl);
            }


            return dierentuin;
        } catch (SQLException | NoRecordFoundException e) {
            e.printStackTrace();
        }
        return dierentuin;
    }

    /**
     * vindListAlleVerschillendeDierenNamen is een methode dat alle verschillende dieren teruggeeft die in het park zijn.
     *
     * @param id Deze id is voor te specifieren dat er maar een record gaat worden opgehaald (wordt ingegeven door de user).
     * @return Een lijst van alle dieren die in de tabel staan.
     */

    public Dierentuin vindListAlleVerschillendeDierenNamen(int id) {
        try {
            con = createConnection(con);
            statement = con.prepareStatement(QuerryDAO.selecteerAll("Dierentuin") + " WHERE id_pk = ?");
            statement.setInt(1, id);
            statement.execute();
            set = statement.getResultSet();
            while (set.next()) {
                return this.aggregateDierentuin();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return null;
    }

    /**
     * vindVerschillendeDierenVanEenDierensoort is een methode om te kijken of een bepaalde dierensoort er is of niet .
     *
     * @param bevat, dit is een parameter die door de gebruiker is ingegeven, daarop wordt gezocht dat deze dierensoort er is of niet.
     * @return Een lijst van de dieren die bevat in hun diernaam hebben.
     */
    public List<Dierentuin> vindVerschillendeDierenVanEenDierensoort(String bevat) {
        List<Dierentuin> dierentuinen = new ArrayList<>();
        try {
            con = createConnection(con);
            statement = con.prepareStatement("Select * from dierentuin.dierentuin tuin LEFT JOIN dierentuin.dier dier on dier.id_dier_pk = tuin.id_dier LEFT JOIN dierentuin.familie fm on fm.id_familie_pk = tuin.id_familie LEFT JOIN dierentuin.geslacht gs on gs.id_geslacht_pk = tuin.id_geslacht LEFT JOIN dierentuin.gif gf on gf.id_gif_pk = tuin.id_gif LEFT JOIN dierentuin.natural_habitat nh on nh.id_habitat_pk = tuin.id_habitat LEFT JOIN dierentuin.woonplaats wp on wp.id_woonplaats_pk = tuin.id_woonplaats LEFT JOIN dierentuin.zoo zo on zo.id_zoo_pk = wp.id_zoo WHERE dier_name Like ?");
            statement.setString(1, "%" + bevat + "%");
            statement.execute();
            set = statement.getResultSet();
            while (set.next()) {
                Dierentuin dierentuin;
                dierentuin = this.aggregateDierentuin();
                dierentuinen.add(dierentuin);
            }
            return dierentuinen;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return Collections.emptyList();
    }


    /**
     * vindDiervanafXGeboortedatum is een methode om te kijken of er bepaalde dieren zijn geboren na een vooraf opgegeven datum.
     *
     * @param datum, dit is een parameter die door de gebruiker is ingegeven, daarop wordt gezocht dat er een geboorte is na deze datum.
     * @return Een lijst van de dieren die geboren zijn na deze datum.
     */
    public List<Dierentuin> vindDiervanafXGeboortedatum(Date datum) {
        List<Dierentuin> dierentuins = new ArrayList<>();
        java.sql.Date dateSql = new java.sql.Date(datum.getTime());
        try {
            con = createConnection(con);
            statement = con.prepareStatement("Select * from dierentuin.dierentuin WHERE geboorte_datum<=?");
            statement.setString(1, String.valueOf(dateSql));
            statement.execute();
            set = statement.getResultSet();
            while (set.next()) {
                Dierentuin dierentuin = aggregateDierentuin();
                dierentuins.add(dierentuin);
            }
            return dierentuins;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return Collections.emptyList();
    }

    /**
     * vindDiervanafXGestorvenZijn is een methode om te kijken of er bepaalde dieren zijn gestorven na een vooraf opgegeven datum.
     * wanneer deze methode wordt getest (methode 7) en een datum ingeeft na 2000 zal er een dir zijn dat gestorven is op 1 januari 2000, meer bepaald stokstaartje Jonas.
     *
     * @param datum, dit is een parameter die door de gebruiker is ingegeven, daarop wordt gezocht dat er een sterfte is na deze datum.
     * @return Een lijst van de dieren die gestorven zijn na deze datum.
     */
    public List<Dierentuin> vindDiervanafXGestorvenZijn(Date datum) {
        List<Dierentuin> dierentuins = new ArrayList<>();
        java.sql.Date dateSql = new java.sql.Date(datum.getTime());
        try {
            con = createConnection(con);
            statement = con.prepareStatement("Select * from dierentuin.dierentuin WHERE sterf_datum<=?");
            statement.setString(1, String.valueOf(dateSql));
            statement.execute();
            set = statement.getResultSet();
            while (set.next()) {
                Dierentuin dierentuin = aggregateDierentuin();
                dierentuins.add(dierentuin);
            }
            return dierentuins;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return Collections.emptyList();
    }

    /**
     * updateEenGeslachtVanEenDier is een functie geschreven voor het geslacht van een dier te veranderen.
     *
     * @param geslachtNummer, dit is een parameter die door de gebruiker onrechtstreeks ingeeft. Dit is namelijk ingegeven door eerst te vragen welk geslacht het is en dit geslacht te veranderen in een nummer, daarop wordt het geslacht in de databank aangepast.
     * @param id,             dit is een parameter die de user ingeeft voor een geslacht te kunnen updaten.
     */
    public void updateEenGeslachtVanEenDier(int geslachtNummer, int id) {
        try {
            insertQuerryForSaveUpdatesToZero();
            con = createConnection(con);
            statement = con.prepareStatement("UPDATE dierentuin.dierentuin SET id_geslacht = ? WHERE id_pk = ?;");
            statement.setInt(1, geslachtNummer);
            statement.setInt(2, id);
            statement.executeUpdate();
            insertQuerryForSaveUpdatesToOne();
            System.out.println("U heeft het record met succes kunnen updaten");
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * updateEenWoonplaatsVanEenDier is een methode die een woonplaats van een dier gaat updaten 'naar verhuisd' met de juiste zoowoonplaats.
     *
     * @param woonplaats_id, is een parameter die door de gebruiker onrechtstreeks ingeeft. Dit is namelijk ingegeven door eerst te vragen naar welke zoo een dier gaat en deze zoo naam te veranderen in een nummer, daarop wordt de woonplaats aangepast in de databank.
     * @param id_pk,         dit is een parameter die de user ingeeft voor een woonplaats van een dier te kunnen updaten, namelijk waar hij wilt.
     */
    public void updateEenWoonplaatsVanEenDier(int woonplaats_id, int id_pk) {
        try {
            insertQuerryForSaveUpdatesToZero();
            con = createConnection(con);
            statement = con.prepareStatement("UPDATE dierentuin.dierentuin SET id_woonplaats = ? WHERE id_pk = ?;");
            statement.setInt(1, woonplaats_id);
            statement.setInt(2, id_pk);
            statement.executeUpdate();
            insertQuerryForSaveUpdatesToOne();
            System.out.println("U heeft het record met succes kunnen updaten");
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * updateEenSterfdatumVanEenDier is een methode die de sterfdatum van een welbepaald dier aanpast.
     *
     * @param id_pk,        dit is een parameter die de user ingeeft voor een sterfdatum te kunnen updaten.
     * @param datumSterfte, dit is een parameter die de user heeft ingegeven namelijk voor deze datum als sterfdatum te kunnen zetten.
     */
    public void updateEenSterfdatumVanEenDier(int id_pk, Date datumSterfte) {
        java.sql.Date dateSql = new java.sql.Date(datumSterfte.getTime());
        try {
            con = createConnection(con);
            statement = con.prepareStatement("UPDATE dierentuin.dierentuin SET sterf_datum = ? WHERE id_pk = ?;");
            statement.setString(1, String.valueOf(dateSql));
            statement.setInt(2, id_pk);
            statement.executeUpdate();
            System.out.println("U heeft het record met succes kunnen updaten");
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * insertEenGeboorte is een methode die de user in staat stelt om een nieuw dier in de databank te zetten.
     *
     * @param idDier,         deze wordt onrechtstreeks door de user ingegeven, we vragen naar welk soort het is en geven dan het overeenkomstige primary key, dit nummer.
     * @param geboorteDatum,  deze parameter heeft de user ingegeven voor de datum in de tabel te kunnen zetten.
     * @param idFamilie,      deze parameter wordt autommatisch ingevuld als het id van het dier(soort) geweten is.
     * @param idWoonplaats,   deze parameter wordt autommatisch ingevuld als het id van het dier(soort) geweten is.
     * @param idHabitat,      deze parameter wordt autommatisch ingevuld als het id van het dier(soort) geweten is.
     * @param idGif,          deze parameter wordt autommatisch ingevuld als het id van het dier(soort) geweten is.
     * @param dieren_naam,    deze parameter is door de user ingegeven (naam van het dier).
     * @param geslachtNummer, deze wordt door de user onrechtstreeks gegeven, het geslacht (zie voor deze methode) wordt omgezet in een nummer.
     */
    public void insertEenGeboorte(int idDier, Date geboorteDatum, int idFamilie, int idWoonplaats, int idHabitat, int idGif, String dieren_naam, int geslachtNummer) {
        java.sql.Date dateSql = new java.sql.Date(geboorteDatum.getTime());
        try {
            con = createConnection(con);
            statement = con.prepareStatement("INSERT INTO dierentuin.dierentuin (id_dier, geboorte_datum, id_familie, id_woonplaats, id_habitat, id_gif, dieren_naam, id_geslacht) VALUES (?,?,?,?,?,?,?,?);");
            statement.setInt(1, idDier);
            statement.setString(2, String.valueOf(dateSql));
            statement.setInt(3, idFamilie);
            statement.setInt(4, idWoonplaats);
            statement.setInt(5, idHabitat);
            statement.setInt(6, idGif);
            statement.setString(7, dieren_naam);
            statement.setInt(8, geslachtNummer);
            statement.executeUpdate();
            System.out.println("U heeft het record met succes kunnen inserten in de databank");
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

