package repo;


import domain.Woonplaats;
import domain.Zoo;
import exeptions.NoRecordFoundException;
import query_utils.QuerryDAO;
import services.ZooService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static database.MySqlConnection.closeConnections;
import static database.MySqlConnection.createConnection;

/**
 * @author Stijn Hesters
 * Dit is de Database Access Object van de tabel woonplaats in de MySQL Workbench.
 */

public class WoonplaatsDAO {

    private Connection con;
    private ResultSet set;
    private PreparedStatement statement;

    public WoonplaatsDAO() {
    }

    protected WoonplaatsDAO(Connection con) {
        this.con = con;
    }

    /**
     * aggregateWoonplaats is een nieuwe methode waar al de namen van de (kolommen, in de tabel) woonplaats in staan.
     *
     * @return Dit is voor alles te kunnen weergeven later dat in de tabel woonplaats staat.
     */
    private Woonplaats aggregateWoonplaats() {
        Woonplaats woonplaats = new Woonplaats();
        try {
            woonplaats.setId_woonplaats_pk(set.getInt("id_woonplaats_pk"));
            woonplaats.setWoonplaats_name(set.getString("woonplaats_name"));

            if (set.getInt("id_zoo") > 0) {
                ZooService zooService = new ZooService();
                Zoo zoo = zooService.vindZooDoorId(set.getInt("id_zoo"));
                woonplaats.setZoo(zoo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
        return woonplaats;
    }

    /**
     * vindAlleVerschillendeWoonplaatsDoorId is een methode dat een woonplaats teruggeeft waar een dier woont.
     *
     * @param woonplaatsIdentificatie, deze parameter wordt ingevuld door aggregateDierentuin.
     * @return Geeft een record weer, namelijk de woonplaats met het opgegeven identificatienummer.
     */
    public Woonplaats vindAlleVerschillendeWoonplaatsDoorId(int woonplaatsIdentificatie) {
        try {
            con = createConnection(con);
            statement = con.prepareStatement("Select * from dierentuin.woonplaats WHERE id_woonplaats_pk = ?");
            statement.setInt(1, woonplaatsIdentificatie);
            statement.execute();
            set = statement.getResultSet();
            if (set.next()) {
                return this.aggregateWoonplaats();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return null;
    }

    /**
     * vindAlleVerschillendeWoonplaatsen is een methode dat alle verschillende woonplaatsen teruggeeft van dieren die in het park leven / geleefd hebben, deze functie is voor aggregateDierentuin..
     *
     * @return Een lijst van al de verschillende woonplaatesen van dieren die in het park geboren leven / geleefd hebben.
     */

    public List<Woonplaats> vindAlleVerschillendeWoonplaatsen() {
        List<Woonplaats> woonplaatsen = new ArrayList<>();
        try {
            con = createConnection(con);
            statement = con.prepareStatement(QuerryDAO.selecteerAll("woonplaats"));
            statement.execute();
            set = statement.getResultSet();
            while (set.next()) {
                Woonplaats woonplaats;
                woonplaats = this.aggregateWoonplaats();
                woonplaatsen.add(woonplaats);
            }
            return woonplaatsen;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return Collections.emptyList();
    }

    /**
     * vindWoonplaatsZooId is een methode voor een bepaalde primary key van een zoo weer te geven.
     *
     * @param idZoo, dit is de input van de gebruiker.
     * @return Een waarde van een primary Key dat overeenkomt met de zoo id (userinput).
     */
    public int vindWoonplaatsZooId(int idZoo) {
        try {
            con = createConnection(con);
            statement = con.prepareStatement("Select * from dierentuin.woonplaats WHERE id_zoo = ?");
            statement.setInt(1, idZoo);
            statement.execute();
            set = statement.getResultSet();
            if (set.next()) {
                return aggregateWoonplaats().getId_woonplaats_pk();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(set, con, statement);
        }
        return 0;
    }

    /**
     * inserEenNieuweZoo is een nieuwe methode die een nieuwe record van woonplaats invoegt in de tabel woonplaats.
     *
     * @param woonplaats_id, dit is een record dat de user onrechtstreeks ingeft door middel van de zoo id die een vorige methode vindWoonplaatsZooId heeft verwerkt.
     */
    public void inserEenNieuweZoo(int woonplaats_id) {
        try {
            con = createConnection(con);
            statement = con.prepareStatement("INSERT INTO dierentuin.woonplaats (woonplaats_name, id_zoo) VALUES ( ?,? )");
            statement.setString(1, "verhuisd");
            statement.setInt(2, woonplaats_id);
            statement.executeUpdate();
            System.out.println("U heeft het record met succes ingevoegd");
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

