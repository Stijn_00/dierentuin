package exeptions;

/**
 * Dit is de klas NoRecordFoundException, deze exceptie zal elke keer ge"thrown"ed worden als er iets niet lukt voor een try uit te voeren (met voorwaarde dat in de catch NoRecordFoundException staat).
 */

public class NoRecordFoundException extends Exception {

    public NoRecordFoundException(String message) {
        super(message);
    }

    public void printUserFriendlyMessage() {
        System.err.println(getMessage());
    }

}
