package services;

import domain.Gif;
import exeptions.NoRecordFoundException;
import repo.GifDAO;

import java.util.List;

public class GifService {
    private GifDAO gifDAO = new GifDAO();

    /**
     * vindGifDoorId is een functie geschreven voor het de functie aggregateDierentuin.
     *
     * @param gifIdentificatie, is hetgeen waarop de functie de tabellen samegvoegt.
     * @return Geeft een record weer, namelijk het gif met het opgegeven identificatienummer.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public Gif vindGifDoorId(int gifIdentificatie) throws NoRecordFoundException {
        Gif gif = gifDAO.vindAlleVerschillendeGifsDoorId(gifIdentificatie);
        if (gif == null) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record met dit " + gifIdentificatie + " in onze giftabel vinden");
        }
        return gif;
    }
}
