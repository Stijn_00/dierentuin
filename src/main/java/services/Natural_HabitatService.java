package services;

import domain.Natural_Habitat;
import exeptions.NoRecordFoundException;
import repo.Natural_HabitatDAO;

import java.util.List;

public class Natural_HabitatService {
    private Natural_HabitatDAO natural_habitatDAO = new Natural_HabitatDAO();

    /**
     * vindHabitatDoorId is een functie geschreven voor het de functie aggregateDierentuin.
     *
     * @param habitatIdentificatie, is hetgeen waarop de functie de tabellen samegvoegt.
     * @return Geeft een record weer, namelijk het natuurlijke habitat met het opgegeven identificatienummer.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public Natural_Habitat vindHabitatDoorId(int habitatIdentificatie) throws NoRecordFoundException {
        Natural_Habitat nH = natural_habitatDAO.vindAlleVerschillendeHabitatsDoorId(habitatIdentificatie);
        if (nH == null) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record met dit " + habitatIdentificatie + " in onze dierentabel vinden");
        }
        return nH;
    }

    /**
     * vindListHabitatsDoorId is een methode dat al de namen van de verschillende habitats teruggeeft dat mogelijk is van dieren in het park.
     *
     * @return Een lijst van al de namen van de verschillende natuurlijke habitat teruggeeft dat mogelijk is van dieren in het park.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public List<Natural_Habitat> vindListHabitatsDoorId() throws NoRecordFoundException {
        List<Natural_Habitat> nH = natural_habitatDAO.vindAlleVerschillendeHabitats();
        if (nH == null || nH.isEmpty()) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record in onze habitattabel vinden");
        }
        return nH;
    }
}


