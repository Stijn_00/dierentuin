package services;

import domain.Dierentuin;
import exeptions.NoRecordFoundException;
import repo.DierentuinDAO;

import java.util.Date;
import java.util.List;

public class DierentuinService {
    private DierentuinDAO dierentuinDAO = new DierentuinDAO();

    /**
     * vindListAlleVerschillendeDierenNamen is een functie die een samenvoeging is van de verschillende tabellen.
     *
     * @param id Deze id is voor te specifieren dat er maar een record gaat worden opgehaald (wordt ingegeven door de user).
     * @return Geeft een lijst van records weer, namelijk alle dieren.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */

    public Dierentuin vindDierDoorId(int id) throws NoRecordFoundException {
        Dierentuin dierentuin = dierentuinDAO.vindListAlleVerschillendeDierenNamen(id);
        if (dierentuin == null) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record in onze dierentuintabel vinden");
        }
        return dierentuin;
    }

    /**
     * vindVerschillendeDieren is een methode om te kijken of een bepaalde dierennaam er is of niet (al dan niet een bepaald deel).
     *
     * @param bevat, dit is een parameter die door de user is ingegeven, daarop wordt gezocht dat dit dierensoort er is of niet.
     * @return Een lijst van de dieren die bevat in hun dierennaam hebben.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public List<Dierentuin> vindListVerschillendeDierenVanEenDierensoort(String bevat) throws NoRecordFoundException {

        List<Dierentuin> dierentuins = dierentuinDAO.vindVerschillendeDierenVanEenDierensoort(bevat);
        if (bevat.isEmpty() || bevat.equals("[]")) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record in onze dierentuintabel vinden met " + " " + " in de naam");
        } else {
            if (dierentuins == null || dierentuins.isEmpty()) {
                throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record in onze dierentuintabel vinden met " + bevat + " in de naam");
            }
            return dierentuins;
        }
    }

    /**
     * vindDiervanafXGeboortedatum is een methode om te kijken of er bepaalde dieren zijn geboren na een vooraf opgegeven datum.
     *
     * @param datum, dit is een parameter die door de gebruiker is ingegeven, daarop wordt gezocht dat er een geboorte is na deze datum.
     * @return Een lijst van de dieren die geboren zijn na deze datum.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public List<Dierentuin> vindDiervanafXGeboortedatum(Date datum) throws NoRecordFoundException {
        List<Dierentuin> dierentuins = dierentuinDAO.vindDiervanafXGeboortedatum(datum);
        if (dierentuins == null || dierentuins.equals("[]") || dierentuins.isEmpty()) {
            throw new NoRecordFoundException("Er zijn nog geen geboortes geweest voor de datum dat u hebt opgegeven");
        }
        return dierentuins;
    }

    /**
     * vindDiervanafXGetorvenzijn is een methode om te kijken of er bepaalde dieren zijn gestorven na een vooraf opgegeven datum.
     *
     * @param datum, dit is een parameter die door de gebruiker is ingegeven, daarop wordt gezocht dat er een sterfte is na deze datum.
     * @return Een lijst van de dieren die gestorven zijn na deze datum.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public List<Dierentuin> vindDiervanafXGetorvenzijn(Date datum) throws NoRecordFoundException {
        List<Dierentuin> dierentuins = dierentuinDAO.vindDiervanafXGestorvenZijn(datum);
        if (dierentuins == null || dierentuins.equals("[]") || dierentuins.isEmpty()) {
            throw new NoRecordFoundException("Er zijn nog geen sterftes geweest voor de datum dat u hebt opgegeven");
        }
        return dierentuins;
    }

    /**
     * updateEenGeslachtVanEenDier is een functie geschreven voor het geslacht van een dier te veranderen.
     *
     * @param geslachtNummer, dit is een parameter die door de gebruiker onretstreeks is ingegeven nemelijk door eerst te vragen welk geslacht het is en dit geslacht te veranderen in een nummer, daarop wordt het geslacht in de databank aangepast.
     * @param id,             dit is een parameter die de user ingeeft voor een geslacht te kunnen updaten.
     */

    public void updateEenGeslachtVanEenDier(int geslachtNummer, int id) {
        dierentuinDAO.updateEenGeslachtVanEenDier(geslachtNummer, id);
    }

    /**
     * updateEenWoonplaatsVanEenDier is een methode die een woonplaats van een dier gaat updaten 'naar verhuisd' met de juiste zoowoonplaats.
     *
     * @param woonplaats_id, is een parameter die door de gebruiker onrechtstreeks ingeeft. Dit is namelijk ingegeven door eerst te vragen naar welke zoo een dier gaat en deze zoo naam te veranderen in een nummer, daarop wordt de woonplaats aangepast in de databank.
     * @param id_pk,         dit is een parameter die de user ingeeft voor een woonplaats van een dier te kunnen updaten, namelijk waar hij wilt.
     */
    public void updateEenWoonplaatsVanEenDier(int woonplaats_id, int id_pk) {
        dierentuinDAO.updateEenWoonplaatsVanEenDier(woonplaats_id, id_pk);
    }

    /**
     * updateEenSterfdatumVanEenDier is een methode die de sterfdatum van een welbepaald dier aanpast.
     *
     * @param id_pk,        dit is een parameter die de user ingeeft voor een sterfdatum te kunnen updaten.
     * @param datumSterfte, dit is een parameter die de user heeft ingegeven namelijk voor deze datum als sterfdatum te kunnen zetten.
     */
    public void updateEenSterfdatumVanEenDier(int id_pk, Date datumSterfte) {
        dierentuinDAO.updateEenSterfdatumVanEenDier(id_pk, datumSterfte);
    }

    /**
     * insertEenGeboorte, is een methode die de user in staat stelt om een nieuw dier in de databank te zetten.
     *
     * @param idDier,         deze wordt onrechtstreeks door de user ingegeven, we vragen naar welk soort het is en geven dan het overeenkomstige primary key, dit nummer.
     * @param geboorteDatum,  deze parameter heeft de user ingegeven voor de datum in de tabel te kunnen zetten.
     * @param idFamilie,      deze parameter wordt autommatisch ingevuld als het id van het dier(soort) geweten is.
     * @param idWoonplaats,   deze parameter wordt autommatisch ingevuld als het id van het dier(soort) geweten is.
     * @param idHabitat,      deze parameter wordt autommatisch ingevuld als het id van het dier(soort) geweten is.
     * @param idGif,          deze parameter wordt autommatisch ingevuld als het id van het dier(soort) geweten is.
     * @param dieren_naam,    deze parameter is door de user ingegeven (naam van het dier).
     * @param geslachtNummer, deze wordt door de user onrechtstreeks gegeven, het geslacht (zie voor deze methode) wordt omgezet in een nummer.
     */
    public void insertEenGeboorte(int idDier, Date geboorteDatum, int idFamilie, int idWoonplaats, int idHabitat, int idGif, String dieren_naam, int geslachtNummer) {
        dierentuinDAO.insertEenGeboorte(idDier, geboorteDatum, idFamilie, idWoonplaats, idHabitat, idGif, dieren_naam, geslachtNummer);
    }
}
