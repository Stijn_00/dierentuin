package services;

import domain.Dier;
import exeptions.NoRecordFoundException;
import repo.DierDAO;

import java.util.List;

public class DierService {
    private DierDAO dierDAO = new DierDAO();

    /**
     * vindDierDoorId is een functie geschreven voor het de functie aggregateDierentuin.
     *
     * @param dierenIdentificatie, is hetgeen waarop de functie de tabellen samegvoegt.
     * @return Geeft een record weer, namelijk het dier met het opgegeven identificatienummer.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public Dier vindDierDoorId(int dierenIdentificatie) throws NoRecordFoundException {
        Dier dier = dierDAO.vindAlleVerschillendeDierenDoorId(dierenIdentificatie);
        if (dier == null) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record met dit " + dierenIdentificatie + " in onze dierentabel vinden");
        }
        return dier;
    }

    /**
     * vindListDierDoorId is een functie gemaakt die alle dierendierensoorten weergeeft die in het park hebben geleefd, leven.
     *
     * @return Een lijst van al de verschillende dierensoorten die in het park leven / geleefd hebben.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public List<Dier> vindListDierDoorId() throws NoRecordFoundException {
        List<Dier> dier = dierDAO.vindAlleVerschillendeDieren();
        if (dier == null || dier.isEmpty()) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record in onze dierentabel vinden");
        }
        return dier;
    }

    /**
     * vindListVerschillendeDieren is een methode om te kijken of een bepaald dierensoorten in het park is of niet.
     *
     * @param bevat, dit is een parameter die door de user is ingegeven, daarop wordt gezocht dat dit dierensoort er is of niet.
     * @return Een lijst van de dierensoorten die bevat in hun naam hebben.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public List<Dier> vindListVerschillendeDieren(String bevat) throws NoRecordFoundException {
        List<Dier> dier = dierDAO.vindVerschillendeDieren(bevat);
        if (bevat.isEmpty() || bevat.equals("[]")) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record in onze dierentabel vinden met " + " " + " in de naam");
        } else {
            if (dier == null || dier.isEmpty()) {
                throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record in onze dierentabel vinden met " + bevat + " in de naam");
            }
            return dier;
        }
    }

    /**
     * vindEenDierenDoorBevat is een methode die Id_dier_pk weergeeft van een specifieke userinput.
     *
     * @param bevatSoort, dit is de userinput die bepaald welke Id_dier_pk er gaat worden weergegeven.
     * @return Een nummer, meer bepaald de primary key van deze tabel.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public int vindEenDierenDoorBevat(String bevatSoort) throws NoRecordFoundException {
        int dier = dierDAO.vindEenDierenDoorBevat(bevatSoort);
        if (dier == 0) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record in onze dierentabel vinden");
        }
        return dier;
    }
}
