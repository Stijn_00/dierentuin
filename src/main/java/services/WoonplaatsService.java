package services;

import domain.Woonplaats;
import exeptions.NoRecordFoundException;
import repo.WoonplaatsDAO;

import java.util.List;

public class WoonplaatsService {

    private WoonplaatsDAO woonplaatsDAO = new WoonplaatsDAO();

    /**
     * vindWoonplaatsDoorId is een functie geschreven voor het de functie aggregateDierentuin.
     *
     * @param woonplaatsIdentificatie, is hetgeen waarop de functie de tabellen samegvoegt.
     * @return Geeft een record weer, namelijk het woonplaats met het opgegeven identificatienummer.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public Woonplaats vindWoonplaatsDoorId(int woonplaatsIdentificatie) throws NoRecordFoundException {
        Woonplaats woonplaats = woonplaatsDAO.vindAlleVerschillendeWoonplaatsDoorId(woonplaatsIdentificatie);
        if (woonplaats == null) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record met dit " + woonplaatsIdentificatie + " in onze dierentabel vinden");
        }
        return woonplaats;
    }

    /**
     * vindListWoonplaatsDoorId is een methode dat alle verschillende woonplaatsen teruggeeft van dieren die in het park leven / geleefd hebben.
     *
     * @return Een lijst van al de verschillende woonplaatesen van dieren die in het park geboren leven / geleefd hebben.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public List<Woonplaats> vindListWoonplaatsDoorId() throws NoRecordFoundException {
        List<Woonplaats> woonplaats = woonplaatsDAO.vindAlleVerschillendeWoonplaatsen();
        if (woonplaats == null || woonplaats.isEmpty()) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record in onze dierentabel vinden");
        }
        return woonplaats;
    }

    /**
     * vindWoonplaatsZooId is een methode voor een bepaalde primary key van een zoo weer te geven.
     *
     * @param idZoo, dit is de input van de gebruiker.
     * @return Een waarde van een primary Key dat overeenkomt met de zoo id (userinput).
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public int vindWoonplaatsZooId(int idZoo) throws NoRecordFoundException {
        int woonplaats = woonplaatsDAO.vindWoonplaatsZooId(idZoo);
        if (woonplaats == 0) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record met dit " + idZoo + "id nummer in onze dierentabel vinden");
        }
        return woonplaats;
    }

    /**
     * inserEenNieuweZoo is een nieuwe methode die een nieuwe record van woonplaats invoegt in de tabel woonplaats.
     *
     * @param woonplaats_id, dit is een record dat de user onrechtstreeks ingeft door middel van de zoo id die een vorige methode vindWoonplaatsZooId heeft verwerkt.
     */
    public void insertEenNieuweZoo(int woonplaats_id) {
        woonplaatsDAO.inserEenNieuweZoo(woonplaats_id);
    }
}
