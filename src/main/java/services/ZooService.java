package services;

import domain.Zoo;
import exeptions.NoRecordFoundException;
import repo.ZooDAO;

import java.util.List;

public class ZooService {
    private ZooDAO zooDAO = new ZooDAO();

    /**
     * vindZooDoorId is een functie geschreven voor het de functie aggregateWoonplaats.
     *
     * @param zooIdentificatie, is hetgeen waarop de functie de tabellen samegvoegt.
     * @return Geeft een record weer, namelijk de zoo met het opgegeven identificatienummer.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public Zoo vindZooDoorId(int zooIdentificatie) throws NoRecordFoundException {
        Zoo zoo = zooDAO.vindAlleVerschillendeZooDoorId(zooIdentificatie);
        if (zoo == null) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record in onze zoo vinden met " + zooIdentificatie + " de tabel");
        }
        return zoo;
    }

    /**
     * vindListZooDoorId is een functie gemaakt die alle zoos weergeeft naarwaar er dieren zijn verhuisd, alfabetisch gesorteerd.
     *
     * @return Een lijst van al de verschillende zoos waarnaar er dieren zijn verhuisd.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public List<Zoo> vindListZooDoorId() throws NoRecordFoundException {
        List<Zoo> zoo = zooDAO.vindAlleVerschillendeZoos();
        if (zoo == null || zoo.isEmpty()) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record in onze zoo vinden");
        }
        return zoo;
    }

    /**
     * inserEenNieuweZoo is een methode om een nieuwe zoo in te voegen in de tabel van zoo.
     *
     * @param nieuweZoo, dit is input van de user, namelijk daarop gaat men een nieuw record invoegen in de tabel zoo.
     */
    public void inserEenNieuweZoo(String nieuweZoo) {
        zooDAO.inserEenNieuweZoo(nieuweZoo);
    }

    /**
     * vindzooPk is een methode om van een bepaalde zoo de primary key te bepalen.
     *
     * @param nieuweZoo, dit is input van de user, namelijk daarop gaat men kijken naar de primary key van dat record.
     * @return Geeft een getal weern, namelijk de primary key van de overeenkomstige zoo.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public int vindzooPk(String nieuweZoo) throws NoRecordFoundException {
        int zoo = zooDAO.vindzooPk(nieuweZoo);
        if (zoo == 0) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record met dit " + nieuweZoo + " nummer in onze dierentabel vinden");
        }
        return zoo;
    }
}
