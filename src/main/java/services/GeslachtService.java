package services;

import domain.Geslacht;
import exeptions.NoRecordFoundException;
import repo.GeslachtDAO;

import java.util.List;

public class GeslachtService {
    private GeslachtDAO geslachtrDAO = new GeslachtDAO();

    /**
     * vindGeslachtDoorId is een functie geschreven voor het de functie aggregateDierentuin.
     *
     * @param geslachtIdentificatie, is hetgeen waarop de functie de tabellen samegvoegt.
     * @return Geeft een record weer, namelijk het geslacht met het opgegeven identificatienummer.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public Geslacht vindGeslachtDoorId(int geslachtIdentificatie) throws NoRecordFoundException {
        Geslacht geslacht = geslachtrDAO.vindAlleVerschillendeGeslachtsDoorId(geslachtIdentificatie);
        if (geslacht == null) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record met dit " + geslachtIdentificatie + " in onze geslachtstabel vinden");
        }
        return geslacht;
    }
}

