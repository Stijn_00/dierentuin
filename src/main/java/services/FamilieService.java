package services;

import domain.Familie;
import exeptions.NoRecordFoundException;
import repo.FamilieDAO;

import java.util.List;

public class FamilieService {
    private FamilieDAO familieDAO = new FamilieDAO();

    /**
     * vindFamilieDoorId is een functie geschreven voor het de functie aggregateWoonplaats.
     * @param familieIdentificatie, is hetgeen waarop de functie de tabellen samegvoegt.
     * @return Geeft een record weer, namelijk de familie met het opgegeven identificatienummer.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public Familie vindFamilieDoorId(int familieIdentificatie) throws NoRecordFoundException {
        Familie familie = familieDAO.vindAlleVerschillendeFamiliesDoorId(familieIdentificatie);
        if (familie == null) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record met dit " + familieIdentificatie + " in onze dierentabel vinden");
        }
        return familie;
    }

    /**
     *vindListfamilieDoorId is een methode dat alle verschillende families teruggeeft die in het park zijn.
     * @return Een lijst van al de verschillende families die in het park zijn.
     * @throws NoRecordFoundException Dit is voor als er een record zou zijn dat null is, dan krijgt de gebruiker de weergegeven exceptie te zien.
     */
    public List<Familie> vindListfamilieDoorId() throws NoRecordFoundException {
        List<Familie> familie = familieDAO.vindAlleVerschillendeFamilies();
        if (familie == null || familie.isEmpty()) {
            throw new NoRecordFoundException("Ooops, er ging iets fout, we konden geen record in onze familietabel vinden");
        }
        return familie;
    }
}
