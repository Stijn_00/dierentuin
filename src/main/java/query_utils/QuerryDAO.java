package query_utils;

//import exeptions.NoGameStartsWithThisLetter;

public class QuerryDAO {

//    public static final String COLNAME = "game";
//    public static final String GAME_NAME_COL = "game_name";
//    public static final String COL_NAME_LOWER = GAME_NAME_COL.toLowerCase();
//
//    public static String findByFirstLetterGame(String begin) throws NoGameStartsWithThisLetter {
//
//        if (GAME_NAME_COL.startsWith(begin.toLowerCase())){
//            StringBuilder builder = new StringBuilder("SELECT * FROM ");
//            builder.append(COLNAME);
//            builder.append(" WHERE ");
//            builder.append(GAME_NAME_COL);
//            builder.append("LIKE ?");
//            return builder.toString();
//        }else {
//            throw new NoGameStartsWithThisLetter("There is no game that starts with " + begin + " in our game");
//        }
//    }
//    package query_utils;
//
//    public class CategoryQueries {
//
//        public static final String COLNAME = "category";
//        public static final String ID_COL = "id";
//        public static final String CAT_COL = "category_name";
//
//        public static String findByIdQuery(int id){
//            StringBuilder builder = new StringBuilder("SELECT * FROM ");
//            builder.append(CAT_COL);
//            builder.append(" WHERE ");
//            builder.append(ID_COL);
//            builder.append(" = ");
//            builder.append(id);
//            return builder.toString();
//        }
//
//        public static String createQuery(String name,String motherName,String fatherName){
//            StringBuilder builder = new StringBuilder("SELECT * FROM ");
//            builder.append(CAT_COL);
//            builder.append(" WHERE ");
//            if(name!=null && !name.isEmpty()){
//                builder.append("name");
//                builder.append(" LIKE '");
//                builder.append(name+"%'");
//                builder.append(" OR ");
//            }
//            if(motherName!=null && !motherName.isEmpty()){
//                builder.append("motherName");
//                builder.append(" LIKE '");
//                builder.append(motherName+"%'");
//                builder.append(" OR ");
//            }
//            if(fatherName!=null && !fatherName.isEmpty()){
//                builder.append("fatherName");
//                builder.append(" LIKE '");
//                builder.append(fatherName+"%'");
//            }
//            return builder.toString();
//        }
//    }

    /**
     * selecteerAll is een methode die een standaardquerry schrijft.
     * @param tabel, dit is de insert van de user voor te specifieren welke tabel hij alles wilt selecteren.
     * @return een String, meer bepaald een samengestelde String die mede samengesteld wordt door de user, voor alles te selecteren van een bepaalde tabel.
     */
    public static String selecteerAll (String tabel) {
        StringBuilder build = new StringBuilder("Select * from ");
        build.append("dierentuin.");
        build.append(tabel);
        return build.toString();
    }
}
