package database;

import java.sql.*;

/**
 * Dit is de klas van connecties naar de databank (maken en sluiten)
 */

public class MySqlConnection {
    private static final String URL = "jdbc:mysql://localhost:3306/dierentuin";
    private static final String URLTEST = "jdbc:mysql://localhost:3306/dierentuinTest";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    private MySqlConnection() {
    }

    /**
     * createConnection is een metode om een connectie te maken naar de databank, dit als hij gesloten is of null is.
     *
     * @param connection Deze parameter is voor wanneer er in de klasse een andere variabele is gegeven aan connection.
     * @return Dit returnd steeds de nieuwe connectie naar de databank, wanneer hij wordt aangeroepen.
     * @throws SQLException Deze exeptie is er voor als het om een of ander reden mislult voor te connecteren naar de databank dat er een melding van komnt.
     */
    public static Connection createConnection(Connection connection) throws SQLException {
        if (connection == null || connection.isClosed()) {
            return DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } else {
            return connection;
        }
    }

    /**
     * createConnectionTest is een metode om een connectie te maken naar de (test)databank, dit als hij gesloten is of null is.
     *
     * @param connection Deze parameter is voor wanneer er in de klasse een andere variabele is gegeven aan connection.
     * @return Dit geeft steeds de nieuwe connectie naar de databank, wanneer hij wordt aangeroepen.
     * @throws SQLException Deze exceptie  is er voor als het om een of ander reden mislukt voor te connecteren naar de databank dat er een melding van komt.
     */
    public static Connection createConnectionTest(Connection connection) throws SQLException {
        if (connection == null || connection.isClosed()) {
            return DriverManager.getConnection(URLTEST, USERNAME, PASSWORD);
        } else {
            return connection;
        }
    }

    /**
     * closeConnections is een metode om na een connectie met de databank te maken hem weer te kunnen sluiten.
     *
     * @param set Deze parameter werd geopend en in deze functie wordt hij gesloten.
     * @param con Deze parameter werd geopend en in deze functie wordt hij gesloten.
     * @param preparedStatement Deze parameter werd geopend en in deze functie wordt hij gesloten.
     */
    public static void closeConnections(ResultSet set, Connection con, PreparedStatement preparedStatement) {
        try {
            if (set != null) {
                set.close();
            }
            preparedStatement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * closeConnectionsTest is een metode om na een connectie met de (Test)databank te maken hem weer te kunnen sluiten.
     *
     * @param con Deze parameter werd geopend en in deze functie wordt hij gesloten.
     */
    public static void closeConnectionsTest(Connection con) {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
